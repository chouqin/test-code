trait User { def name: String }

trait Tweeter extends User {
  //user: User => 

  def tweet(msg: String) = println(s"$name: $msg")
}

trait Wrong extends Tweeter {
  def noCanDo = name
}
