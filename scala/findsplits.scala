import scala.collection.mutable.ArrayBuffer
 
object FindSplits {
 
  def findSplits(arr: Array[Double], numSplits: Int): Array[Double] = {
    // 构建value，count数组
    // arr must be sorted first
    def getValueCount(arr: Array[Double]): Array[(Double, Int)] = {
      val valueCount = new ArrayBuffer[(Double, Int)]
      var index = 1
      var currentValue = arr(0)
      var currentCount = 1
      while (index < arr.length) {
        if (currentValue != arr(index)) {
          valueCount.append((currentValue, currentCount))
          currentCount = 1
          currentValue = arr(index)
        } else {
          currentCount += 1
        }
        index += 1
      }
      valueCount.append((currentValue, currentCount))
 
      valueCount.toArray
    }
 
    val valueCount = getValueCount(arr)
    if (valueCount.length <= numSplits) {
      return valueCount.map(_._1)
    }
 
    // stride between splits
    val stride: Double = arr.length.toDouble / (numSplits + 1)
    println("stride = " + stride)

    // iterate `valueCount` to find splits
    val splits = new ArrayBuffer[Double]
    var index = 1
    // currentCount: sum of counts of values that have been visited
    var currentCount = valueCount(0)._2
    // expectedCount: expected value for `currentCount`.
    // If `currentCount` is closest value to `expectedCount`,
    // then current value is a split threshold.
    // After finding a split threshold, `expectedCount` is added by stride.
    var expectedCount = stride
    while (index < valueCount.length) {
      // If adding count of current value to currentCount
      // makes currentCount less close to expectedCount,
      // previous value is a split threshold.
      if (math.abs(currentCount - expectedCount) <
        math.abs(currentCount + valueCount(index)._2 - expectedCount)) {
        splits.append(valueCount(index-1)._1)
        expectedCount += stride
      }
      currentCount += valueCount(index)._2
      index += 1
    } 
    splits.toArray
  }
 
 
  def main(arr: Array[String]) {
    val arr = Array(2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 4, 5).map(_.toDouble)    
    val splits = findSplits(arr, 2)
    println(splits.mkString(" "))
 
 
 
    //val arr2 = Array.fill(200000)(math.random).sorted
    //val splits2 = findSplits(arr2, 5)
    //println(splits2.mkString(" "))
  }
 
}
