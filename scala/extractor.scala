import scala.util.matching.Regex

object Email {
  def unapply(str: String) = new Regex("""(.*)@(.*)""")
    .unapplySeq(str).get match {
    case user :: domain :: Nil => Some(user, domain)
    case _ => None
  }
}

"userdomain.com" match {
  case Email(user, domain) => println(user + "@" + domain)
  case _ => println("don't match email")
}
