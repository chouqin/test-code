val xmax, ymax = 100 // 把xmax, ymax同时设为100

var greeting, message: String = null // 注意如何声明类型


// how to import
//
import scala.math._ // or simply import math._
sqrt(2)

// Or
math.sqrt(2)

"Hello"(4) // a shortcut for "Hello".apply(4)

// 对于函数调用
print "Hello" // 会报错
print("hello") // 就不会有问题
list.foreach(print _) // OK 
list.foreach(print(_)) // OK
