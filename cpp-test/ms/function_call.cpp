#include <iostream>
#include <string>
#include <stdexcept>
#include <vector>
#include <stack>
#include <map>
#include <algorithm>

using namespace std;

struct sort_pred {
  bool operator()(const pair<string,string> &left, const pair<string,string> &right) {
    return left.second < right.second;
  }
};

struct Record {
  string name;
  string start;
  string end;
  const Record * prev;
  int depth;

  Record(string n, string s, string e, const Record * p = NULL, int d = 0):
    name(n), start(s), end(e), prev(p), depth(d) {}
};

bool contains(const Record * top, string start, string end) {
  if (start <= top->end) {
    if (end > top->end) {
      throw invalid_argument("overlap interval");
    }
    return true;
  } else {
    return false;
  }
}

string output_path(const Record * record) {
  if (record->prev == NULL) {
    return record->name + " " + record->start + " " + record->end;
  } else {
    return output_path(record->prev) + "-->" + record->name + " " + record->start + " " + record->end;
  }
}

string output(const vector<Record *>& records) {
  int max_depth = 0;
  int max_index = -1;
  for (vector<Record *>::size_type i = 0; i < records.size(); ++ i) {
    if (records[i]->depth > max_depth) {
      max_depth = records[i]->depth;
      max_index = i;
    }
  }
  return output_path(records[max_index]);
}

string function_call(const vector< pair<string, string> > & start_actions,
                     const map<string, string> & end_actions) {
  vector<Record *> records;
  stack<Record *> path;

  for (vector< pair<string, string> >::const_iterator it = start_actions.begin(); it < start_actions.end(); ++it) {
    string name = it->first;
    string start = it->second;
    string end = end_actions.find(name)->second;

    // 把当前的元素插入栈
    while (!(path.empty()) && !contains(path.top(), start, end)) {
      path.pop();
    }
    Record * r = NULL;
    // 当前栈中没有元素
    if (path.empty()) {
      r = new Record(name, start, end);
    } else {
      Record * top = path.top();
      r = new Record(name, start, end, top, top->depth + 1);
    }
    records.push_back(r);
    path.push(r);
  }
  return output(records);
}


int main() {
  int n;
  cin >> n;
  vector< pair<string, string> > start_actions;
  map<string, string> end_actions;
  try {
    for (int i = 0; i < n; ++ i) {
      string name, time, action;
      cin >> name >> time >> action;
      if (action == "START") {
        start_actions.push_back(make_pair(name, time));
      } else if (action == "END") {
        end_actions.insert(make_pair(name, time));
      } else {
        throw invalid_argument("unkown action");
      }
    }
    sort(start_actions.begin(), start_actions.end(), sort_pred());
    cout << function_call(start_actions, end_actions) << endl;
  } catch (const invalid_argument & e) {
    cout << "Incorrect performance log" <<endl;
  }
}
