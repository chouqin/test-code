#include <iostream>
#include <stdexcept>
#include <string>
using namespace std;

int char2int(char c) {
  if ((c >= '0') && (c <= '9')) {
    return c - '0';
  } else if ((c >= 'a') && (c <= 'z')){
    return c - 'a' + 10;
  } else {
    throw invalid_argument("invalid char");
  }
}

char int2char(int i) {
  if (i >= 10 && i < 36) {
    return i - 10 + 'a';
  } else {
    return i + '0';
  }
}

string recorder_string(const string & line) {
  int * count = new int[36];
  for (int i = 0; i < 36; ++ i) {
    count[i] = 0;
  }

  for (int i = 0; i < line.length(); ++i) {
    int index = char2int(line[i]);
    count[index] += 1;
  }

  bool flag = true;
  string result = "";
  while(flag) {
    flag = false;
    for (int i = 0; i < 36; ++i) {
      if (count[i] >= 1) {
        count[i] -= 1;
        result += int2char(i);
        if (count[i] >= 1) {
          flag = true;
        }
      }
    }
  }
  return result;
}

int main() {
  for (string line; getline(cin, line);) {
    try {
      cout << recorder_string(line) << endl;
    } catch (const invalid_argument& e) {
      cout << "<invalid input string>" << endl;
    }
  }
  return 0;
}
