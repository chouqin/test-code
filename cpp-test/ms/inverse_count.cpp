#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>
#include <sstream>

using namespace std;

int compare(int a, int b) {
  if (a == b) {
    return 0;
  } else if (a > b) {
    return 1;
  } else {
    return -1;
  }
}

int inverse_count(const vector<int> & arr) {
  int n = (int)arr.size();

  int max_count = 0;
  for (int i = 0; i < n; ++ i) {
    for (int j = i+1; j < n; ++j) {
      int count = compare(arr[i], arr[j]);
      for (int k = i+1; k < j; ++k) {
        count += compare(arr[i], arr[k]);
        count += compare(arr[k], arr[j]);
      }
      if (count > max_count) {
        max_count = count;
      }
    }
  }

  return max_count;
}

int main() {
  for (string line; getline(cin, line); ) {
    stringstream ss(line);
    string s;
    vector<int> arr;
    while (getline(ss, s, ',')) {
      arr.push_back(atoi(s.c_str()));
    }
    cout << inverse_count(arr) << endl;
  }
}
