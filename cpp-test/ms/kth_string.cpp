#include <iostream>
#include <stdexcept>

using namespace std;

// 这个函数花了好久才写队，:b，组合公式已经忘光了。
int combinations(int a, int b) {
  if (b > (a / 2)) {
    b = a - b;
  }

  long long result = 1;
  for (int i = 0; i < b; ++i) {
    result *= a - i;
  }
  for (int i = 1; i <= b; ++i) {
    result /= i;
  }

  return (int) result;
}

string kth_string(int n, int m, int k) {
  int sum = m + n;

  // 处理特殊情况
  if ((m == 0) || (n == 0)) {
    string result = "";
    for (int i = 0; i < n; ++i) {
      result += '0';
    }
    for (int i = 0; i < m; ++i) {
      result += '1';
    }
    return result;
  }

  int total = combinations(sum, m);
  cout << k << " " <<  sum << " " << m << " " << total << endl;
  if (k > total) {
    throw invalid_argument("impossible");
  }

  int current_sum = 0;
  int highest_one = m;
  for (;highest_one <= sum; ++ highest_one) {
    int new_sum = current_sum + combinations(highest_one-1, m-1);
    if (new_sum >= k) {
      break;
    }
    current_sum = new_sum;
  }

  string result = "";
  for (int i = sum; i > highest_one; -- i) {
    result += '0';
  }
  result += '1';
  result += kth_string(highest_one-m, m-1, k-current_sum);

  return result;
}

int main() {
  //cout << combinations(4, 2) << endl;
  int t;
  cin >> t;
  for (int i = 0; i < t; ++ i) {
    int n, m, k;
    cin >> n >> m >> k;
    try {
      cout << kth_string(n, m, k) << endl;
    } catch (const invalid_argument & e) {
      cout << "Impossible" << endl;
    }
  }

  return 0;
}
