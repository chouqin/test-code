#include <iostream>
#include <vector>
#include <queue>
#include <cstdio>
using namespace std;

int abs(int a, int b) {
  return a > b ? (a - b) : (b - a);
}

int min(int a, int b) {
  return a < b ? a : b;
}

int get_dist(int x1, int y1, int x2, int y2) {
  return min(abs(x1, x2), abs(y1, y2));
}

struct Dist {
    int id; // use to index
    int dist;

    Dist(int i, int d): id(i), dist(d) {}
};

struct CompareDist {
    bool operator() (Dist &a, Dist &b) {
        return a.dist > b.dist;
    }
};

int min_dist(int ** d, int n) {
  priority_queue<Dist, vector<Dist>, CompareDist> dists;
  dists.push(Dist(0, 0));
  bool * visited = new bool[n];
  for (int i = 0; i < n; ++i) {
    visited[i] = false;
  }
  int max = d[0][n-1];
  while (!dists.empty()) {
    Dist node = dists.top();
    dists.pop();
    int current = node.id;
    //cout << "current max is " << max << endl;
    //cout << "get from queue: "<< current << " "<< node.dist << endl;

    visited[current] = true;

    if (current == (n-1)) {
      return node.dist;
    }

    for (int i = 0; i < n; ++i) {
      if (visited[i]) {
        continue;
      }
      int dist = node.dist + d[current][i];
      if (dist >= max) {
        continue;
      }
      //cout << "push to q: " << i << " " << dist << endl;
      dists.push(Dist(i, dist));
    }
  }
}


int main() {
  int n;
  scanf("%d", &n);

  int ** d = new int*[n];
  for (int i = 0; i < n; ++ i) {
    d[i] = new int[n];
  }
  int * x = new int[n];
  int * y = new int[n];
  for (int i = 0; i < n; ++ i) {
    scanf("%d", &x[i]);
    scanf("%d", &y[i]);
    d[i][i] = 0;
  }

  for (int i = 0; i < n; ++ i) {
    for (int j = i+1; j < n; ++j) {
      d[i][j] = d[j][i] = get_dist(x[i], y[i], x[j], y[j]);
    }
  }

  //for (int i = 0; i < n; ++i) {
    //for (int j = 0; j < n; ++j) {
      //cout << d[i][j] << " ";
    //}
    //cout << endl;
  //}
  //
  printf("%d\n", min_dist(d, n));

  //cout << min_dist(d, n) << endl;


  for (int i = 0; i < n; ++ i) {
    delete [] d[i];
  }
  delete [] d;
  delete [] x;
  delete [] y;

  return 0;
}
