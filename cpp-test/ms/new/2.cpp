#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <cstdio>
using namespace std;

struct Module {
  int id;
  vector<int> signals;
  Module(int i): id(i){}
};

void get_counts(const map<int, vector<Module*> > &s2m, queue<int> & signals, int n) {
  int * counts = new int[n];
  for (int i = 0; i < n; ++ i) {
    counts[i] = 0;
  }
  while (!(signals.empty())) {
    int s = signals.front();
    //cout << "get from q: " << s << endl;
    signals.pop();

    if (s2m.count(s) == 0) {
      continue;
    }

    const vector<Module*> &ss = s2m.find(s)->second;
    for (vector<Module*>::const_iterator it = ss.begin(); it != ss.end(); ++ it) {
      const Module* p = *it;
      counts[p->id] += 1;
      for (vector<int>::const_iterator iit = p->signals.begin(); iit != p->signals.end(); ++ iit) {
        //cout << "push to q: "<< *iit << endl;
        signals.push(*iit);
      }
    }
  }

  for (int i = 0; i < n; ++ i) {
    printf("%d ", counts[i]);
  }
  printf("\n");

  delete [] counts;
}

int main() {
  int t;
  scanf("%d", &t);

  for (int i = 0; i < t; ++ i) {
    int n, m;
    scanf("%d", &n);
    scanf("%d", &m);

    queue<int> signals;
    for (int j = 0; j < m; ++ j) {
      int s;
      scanf("%d", &s);
      //cout << "push to q: "<< s << endl;
      signals.push(s);
    }

    map<int, vector<Module*> > s2m;
    Module ** ms = new Module* [n];
    for (int j = 0; j < n; ++ j) {
      int s, k;
      scanf("%d", &s);
      scanf("%d", &k);
      Module * m = new Module(j);
      ms[j] = m;
      for (int p = 0; p < k; ++p) {
        int e;
        scanf("%d", &e);
        m->signals.push_back(e);
      }
      // if not contain, new
      if (s2m.count(s) == 0) {
        vector<Module*> elems;
        elems.push_back(m);
        s2m.insert(make_pair(s, elems));
      } else {
        s2m.find(s)->second.push_back(m);
      }
    }

    get_counts(s2m, signals, n);

    for (int j = 0; j < n; ++j) {
      delete ms[j];
    }
    delete [] ms;
  }

  return 0;
}
