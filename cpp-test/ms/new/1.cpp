#include <iostream>
using namespace std;

int diff(int a, int b) {
  if (a > b) {
    return a - b;
  } else {
    return b - a;
  }
}

bool is_among(int x, int y, int z, int c) {
  return c == x || c == y || c == z;
}

bool to_empty(int x, int y, int z, int dx, int dy, int dz) {
  bool flag = false;
  set<int> s1 = {x, y, z};
  set<int> s2 = {dx, dy, dz};
  return s1 == s2;
}

int main() {
  int x, y, z;
  string seq;
  cin >> x >> y >> z;

  int max = 0;
  int current = 0;
  int cr = 0, cy = 0, cb = 0;
  char c;
  while (cin) {
    cin >> c;
    if (c == '\n') {
      break;
    }
    current += 1;
    if (current > max) {
      max = current;
    }
    if (c == 'R') {
      cr += 1;
    } else if (c == 'B') {
      cb += 1;
    } else {
      cy += 1;
    }

    int dx = diff(cy, cb);
    int dy = diff(cb, cr);
    int dz = diff(cr, cy);
    if (to_empty(x, y, z, dx, dy, dz)) {
      current = 0;
      cy = cb = cr = 0;
    }
  }

  cout << max << endl;


  return 0;
}
