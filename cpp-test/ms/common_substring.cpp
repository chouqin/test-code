#include <iostream>
#include <string>

using namespace std;

int ** new_dp(int m, int n) {
  int **dp = new int*[m];
  for (int i = 0; i < m; ++ i) {
    dp[i] = new int[n];
    for (int j = 0; j < n; ++ j) {
      dp[i][j] = 0;
    }
  }
  return dp;
}

void clear_dp(int ** dp, int m, int n) {
  for (int i = 0; i < m; ++i) {
    delete [] dp[i];
  }
  delete [] dp;
}

void print_dp(int ** dp, int m, int n) {
  for (int i = 0; i < m; ++i) {
    for (int j = 0; j < n; ++ j) {
      cout << dp[i][j] << " ";
    }
    cout << endl;
  }
}

int ** transform_dp(int ** dp, int m, int n) {
  int ** dp2 = new_dp(m, n);
  for (int i = 0; i < m; ++i) {
    for (int j = 0; j < n; ++j) {
      int count = dp[i][j];
      if (count >= 3) {
        dp2[i-count+1][j-count+1] = count;
      }
    }
  }
  cout << "dp2 is:" << endl;
  print_dp(dp2, m, n);
  return dp2;
}

int max(int a, int b) {
  return a > b ? a : b;
}

int ** compute_max(int ** dp, int m, int n) {
  int ** dp3 = new_dp(m, n);
  dp3[0][0] = dp[0][0];
  for (int i = 1; i < m; ++ i) {
    dp3[i][0] = dp3[i-1][0] + dp[i][0];
  }
  for (int j = 1; j < n; ++j) {
    dp3[0][j] = dp3[0][j-1] + dp[0][j];
  }

  for (int i = 1; i < m; ++ i) {
    for (int j = 1; j < n; ++ j) {
      dp3[i][j] = max(dp3[i][j-1], dp3[i-1][j]) + dp[i][j];
    }
  }

  cout << "dp3 is:" << endl;
  print_dp(dp3, m, n);
  return dp3;
}

int get_max(int ** dp, int m, int n) {
  int max = 0;
  for (int i = 0; i < m; ++i) {
    for (int j = 0; j < n; ++j) {
      if (dp[i][j] > max) {
        max = dp[i][j];
      }
    }
  }

  return max;
}

int common_substring(string a, string b) {
  int m = a.length(), n = b.length();
  int ** dp = new_dp(m, n);
  for (int i = 0; i < m; ++ i) {
    dp[i][0] = a[i] == b[0] ? 1 : 0;
  }
  for (int j = 0; j < n; ++j) {
    dp[0][j] = b[j] == a[0] ? 1: 0;
  }
  for (int i = 1; i < m; ++i) {
    for (int j = 1; j < n; ++j) {
      if (a[i] == b[j]) {
        dp[i][j] = dp[i-1][j-1] + 1;
      } else {
        dp[i][j] = 0;
      }
    }
  }
  cout << "dp is:" << endl;
  print_dp(dp, m, n);


  int ** dp2 = transform_dp(dp, m, n);
  clear_dp(dp, m, n);

  int ** dp3 = compute_max(dp2, m, n);
  clear_dp(dp2, m, n);

  int max = get_max(dp3, m, n);
  clear_dp(dp3, m, n);

  return max;
}

int main() {
  string a, b;
  cin >> a >> b;

  cout << common_substring(a, b) << endl;
  return 0;
}
