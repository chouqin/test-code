#include <iostream>
using namespace std;
class Base
{
public:
    virtual ~Base()
    {
        cout << "~Base" << endl;
    }
};
class Dri: public Base
{
public:
    ~Dri()
    {
        cout << "~Dri" << endl;
    }
};
int main()
{
    Base *b = new Dri();
    delete b;
    return 0;
}
