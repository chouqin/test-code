#include <iostream>
using namespace std;

/*
 * 使用模板元编程的技术实现阶乘函数
 * */

// 递归定义
template <unsigned N>
int fac() {
  return fac<N-1>() * N;
}

// 特化
template <>
int fac<0>() {
  return 1;
}

int main() {
  cout << fac<5>() << endl;
  return 0;
}
