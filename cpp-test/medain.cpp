#include <iostream>
#include <cstdio>
using namespace std;

int findK(int a[], int a_begin, int a_end, int b[], int b_begin, int b_end, int k) {
  //cout << "find K get called" << endl;
  //printf("%d %d %d\n", a_begin, b_begin, k);
  if (a_begin == a_end) {
    return b[b_begin + k - 1];
  }
  if (b_begin == b_end) {
    return a[a_begin + k - 1];
  }
  if (k == 1) {
    return a[a_begin] < b[b_begin] ? a[a_begin] : b[b_begin];
  }

  int anum = k / 2;
  int bnum = k - anum;

  if (anum > (a_end - a_begin)) {
    anum = a_end - a_begin;
  }
  if (bnum > (b_end - b_begin)) {
    bnum = b_end - b_begin;
  }

  int aindex = a_begin + anum - 1;
  int bindex = b_begin + bnum - 1;
  //printf("%d %d %d %d\n", aindex, bindex, anum, bnum);

  if (a[aindex] == b[bindex]) {
    //cout << "case 1, return " << a[aindex] << endl;
    return a[aindex];
  } else if (a[aindex] < b[bindex]) {
    int remain = k - anum;
    return findK(a, aindex + 1, a_end, b, b_begin, b_end, remain);
  } else {
    int remain = k - bnum;
    return findK(a, a_begin, a_end, b, bindex+1, b_end, remain);
  }
}

double findMedianSortedArrays(int A[], int m, int B[], int n) {
  int even = ((m + n) % 2) == 0;
  if (even) {
    int a1 = findK(A, 0, m, B, 0, n, (m + n) / 2);
    int a2 = findK(A, 0, m, B, 0, n, (m + n) / 2 + 1);
    return ((double) (a1 + a2)) / 2;
  } else {
    return (double) findK(A, 0, m, B, 0, n, (m + n + 1) / 2);
  }
}


int main() {
  int t;
  cin >> t;
  for (int i = 0; i < t; ++i) {
    int m, n;
    cin >> m >> n;
    int * A = new int[m];
    int * B = new int[n];
    for (int j = 0; j < m; ++j) {
      cin >> A[j];
    }
    for (int j = 0; j < n; ++j) {
      cin >> B[j];
    }
    cout << findMedianSortedArrays(A, m, B, n) << endl;

    delete [] A;
    delete [] B;
  }
}
