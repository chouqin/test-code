#include <iostream>
using namespace std;

struct Node {
  int value;
  Node* next;

  Node(int v, Node* n = NULL): value(v), next(n) {}
};

Node * inverse_list(Node * head) {
  Node * prev = NULL;
  Node * current = head;
  while (current != NULL) {
    Node * next = current->next;
    current->next = prev;
    prev = current;
    current = next;
  }

  return prev;
}

void print_list(Node * head) {
  while (head != NULL) {
    cout << head->value << " ";
    head = head->next;
  }
  cout << endl;
}

int main() {
  Node * n3 = new Node(3);
  Node * n2 = new Node(2, n3);
  Node * n1 = new Node(1, n2);

  print_list(n1);

  Node * head = inverse_list(n1);
  print_list(head);
}
