#include <iostream>
#include <queue>
#include <tuple>

using namespace std;

int main() {
    queue<vector<int>> q;
    vector<int> begin = {};
    q.push(begin);    
    for (int i = 0; i < 10; ++i) {
        const vector<int>& p = q.front(); q.pop();
        
        for (int i: p) {
            cout << i << " ";
        }
        cout << endl;

        vector<int> path = p;        
        path.push_back(i);
        q.push(std::move(path));
    }
    
    const vector<int>& p = q.front(); q.pop();

    for (int i: p) {
        cout << i << " ";
    }
    cout << endl;
}
