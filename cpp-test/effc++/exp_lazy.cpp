#include <cstdio>

struct Exp {
  virtual float Eval(int i) const = 0;
};

struct BinaryAddExp: public Exp {
  const Exp& lhs;
  const Exp& rhs;

  BinaryAddExp(const Exp& lhs, const Exp& rhs)
    : lhs(lhs), rhs(rhs) {}

  inline float Eval(int i) const {
    return lhs.Eval(i) + rhs.Eval(i);
  }
};

struct Vec: public Exp {
  int len;
  float* dptr;

  Vec(void) {}
  Vec(float *dptr, int len)
    : len(len), dptr(dptr) {}

  inline Vec& operator=(const Exp& src) {
    for (int i = 0; i < len; ++i) {
      dptr[i] = src.Eval(i);
    }
    return *this;
  }

  inline float Eval(int i) const {
    return dptr[i];
  }
};

inline BinaryAddExp operator+(const Exp& lhs, const Exp& rhs) {
  return BinaryAddExp(lhs, rhs);
};

const int n = 3;
int main(void) {
  float sa[n] = {1, 2, 3};
  float sb[n] = {2, 3, 4};
  float sc[n] = {3, 4, 5};
  Vec A(sa, n), B(sb, n), C(sc, n);
  // run expression, this expression is longer:)
  A = B + C + C;
  for (int i = 0; i < n; ++i) {
    printf("%d:%f == %f + %f + %f\n", i,
           A.dptr[i], B.dptr[i],
           C.dptr[i], C.dptr[i]);
  }
  return 0;
}
