#include <cstdio>
#include <string>

// 测试C++对于const的保证
//
//
class A {
  int a;
public:
  A() {
    a = 0;
  }

  // 这里不声明为const的话，B::setA不能调用本方法
  void setA(int i) {
    // 声明为const之后，就不能改变当前对象的状态
    a = i;
    // nothing to do
  }

  int getA() const {
    return a;
  }
};

class B {
  // 这里加或者不加const都会
  A a;
public:
  B(): a() {

  }

  // 在当前的函数声明为const之后，
  // 在调用a.setA(i)时，只能调用a的const方法(把a作为一个const传进去)
  void setA(int i) const {
    //a.setA(i);
  }

  // 编译不通过, 不能返回non-const ref
  const A & getARef() const {
    return a;
  }

  int getA() const {
    return a.getA();
  }
};

int main() {
  B b;

  //b.setA(5);
  // 可以通过const_cast将其转成非const的，然后进行修改
  A& aRef = const_cast<A&>(b.getARef());
  aRef.setA(5);
  //b.getARef().setA(5);

  printf("%d\n", b.getA());


  // 测试const string
  const std::string s = "Hello World!";

  //s[0] = 'h'; // 不能改变s的值，因为s[0]调用的是const operator方法，返回的是const reference。
  //s = "Another String"; // 也不能把s设为其它的string，因为s是const的。
  // 这样的话，相当于C里面的同时声明指针不可变，且其指向的值不可变。

  printf("%s\n", s.c_str()); // 不使用c_str()会报错

  return 0;
}
