#include <iostream>
using namespace std;

//template <unsigned n>
//struct Factorial {
  //static const int value = n * Factorial<n-1>::value;
//};

//template <>
//struct Factorial<0> {
  //static const int value = 1;
//};

//int main() {
  //cout << Factorial<5>::value << endl;
//}


template <unsigned n>
unsigned factorial() {
  return n * factorial<n-1>();
}

template <>
unsigned factorial<0>() {
  return 1;
}

int main() {
  cout << factorial<5>() << endl;
}
