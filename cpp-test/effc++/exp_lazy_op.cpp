#include <cstdio>

struct Exp {
  virtual float Eval(int i) const = 0;
};

struct mul {
  inline static float Map(float a, float b) {
    return a * b;
  }
};

template<typename OP>
struct BinaryMapExp: public Exp {
  const Exp& lhs;
  const Exp& rhs;

  BinaryMapExp(const Exp& lhs, const Exp& rhs)
    : lhs(lhs), rhs(rhs) {}

  inline float Eval(int i) const {
    return OP::Map(lhs.Eval(i), rhs.Eval(i));
  }
};

struct Vec: public Exp {
  int len;
  float* dptr;

  Vec(void) {}
  Vec(float *dptr, int len)
    : len(len), dptr(dptr) {}

  inline Vec& operator=(const Exp& src) {
    for (int i = 0; i < len; ++i) {
      dptr[i] = src.Eval(i);
    }
    return *this;
  }

  inline float Eval(int i) const {
    return dptr[i];
  }
};

template<typename OP>
inline BinaryMapExp<OP> F(const Exp& lhs, const Exp& rhs) {
  return BinaryMapExp<OP>(lhs, rhs);
}

inline BinaryMapExp<mul> operator*(const Exp& lhs, const Exp& rhs) {
  return F<mul>(lhs, rhs);
}

struct maximum{
  inline static float Map(float a, float b) {
    return a > b ? a : b;
  }
};


const int n = 3;
int main(void) {
  float sa[n] = {1, 2, 3};
  float sb[n] = {2, 3, 4};
  float sc[n] = {3, 4, 5};
  Vec A(sa, n), B(sb, n), C(sc, n);
  // run expression, this expression is longer:)
  A = B * F<maximum>(C, B);
  for (int i = 0; i < n; ++i) {
    printf("%d:%f == %f * max(%f, %f)\n",
           i, A.dptr[i], B.dptr[i], C.dptr[i], B.dptr[i]);
  }
  return 0;
}
