#include <iostream>
using namespace std;

template <typename T> class Rational;

template <typename T>
const Rational<T> doMul(const Rational<T>& lhs,
                            const Rational<T>& rhs);

template <typename T>
class Rational {
public:
  friend Rational operator*(const Rational& lhs,
                                  const Rational& rhs) {
    return doMul(lhs, rhs);
  }

  Rational(const T& numerator = 0,
           const T& denominator = 1):
    numerator_(numerator), denominator_(denominator) {}

  const T numerator() const {
    return numerator_;
  }
  const T denominator() const {
    return denominator_;
  }

private:
  T numerator_;
  T denominator_;
};

template <typename T>
const Rational<T> doMul(const Rational<T>& lhs,
                            const Rational<T>& rhs) {
  return Rational<T>(lhs.numerator() * rhs.numerator(),
                  lhs.denominator() * rhs.denominator());
}

int main() {
  Rational<int> oneHalf(1, 2);
  Rational<int> result = 2 * oneHalf;
  cout << result.numerator() << " " << result.denominator() << endl;

  return 0;
}
