#include <iostream>

using namespace std;


int count_num(int * a, int begin, int end) {
    if (end <= (begin + 1)) return 0;
    int mid = (begin + end ) / 2;
    int count1 = count_num(a, begin, mid);
    int count2 = count_num(a, mid, end);
    //cout << mid  << " "<< begin  << " " << end << endl;

    int index1 = begin;
    int index2 = mid;
    int * tmp = new int[end - begin];
    int index = 0;
    int count = 0;
    while ((index1 < mid) && (index2 < end)) {
        if (a[index1] <= a[index2]) {
            tmp[index] = a[index1];
            index1 += 1;
        } else {
            tmp[index] = a[index2];
            count += (mid - index1);
            index2 += 1;
        }
        index += 1;
    }
    while (index1 < mid) {
        tmp[index++] = a[index1++];
    }
    while (index2 < end) {
        tmp[index++] = a[index2++];
    }

    //cout << "current tmp is: ";
    //for (int i = 0; i < index; ++ i) {
      //cout << tmp[i] << " ";
    //}
    //cout << endl;


    for (index = begin; index < end; index++) {
        a[index] = tmp[index-begin];
    }

    //cout << "current a is: ";
    //for (int i = begin; i < end; ++ i) {
      //cout << a[i] << " ";
    //}
    //cout << endl;

    //cout << "current count is " << count << " " << count1  << " " << count2 << endl;

    delete [] tmp;
    return count + count1 + count2;
}

int main(void) {
    int n;
    cin >> n;
    int * arr = new int[n];

    for (int i = 0; i < n; ++ i) {
        cin >> arr[i];
    }

    cout << count_num(arr, 0, n) << endl;

    for (int i = 0; i < 10; ++ i) {
      cout << arr[i] << endl;
    }

    delete [] arr;
    return 0;
}
