#include <iostream>
using namespace std;


bool isMatch(const char *s, const char *p) {
  //string ss(s), sp(p);
  //int m = s.length, n = p.length;
  //bool **flag = new *flag [m];
  //for (int i = 0; i < m; ++i) {
    //flag[i] = new flag[n];
    //for (int j = 0; j < n; ++j) {
      //flag[j] = false;
    //}
  //}


  //for (int i = 0; i < m; ++i) {
    //delete [] flag[i];
  //}
  //delete [] flag;

  if ((*s == '\0') && (*p == '\0')) {
    return true;
  }
  if (*p == '*') {
    while (*p == '*') {
      p++;
    }
    const char new_s = s;
    while (*s) {
      if (isMatch(s, p)) {
        return true;
      }
      s ++;
    }
    if ((*s == '\0') && (*p == '\0')) {
      return true;
    }
    return false;
  }
  if ((*s == *p) || (*p == '?')) {
    return isMatch(++s, ++p);
  }
  return false;
}

int main() {
  cout << isMatch("aaabbbaabaaaaababaabaaabbabbbbbbbbaabababbabbbaaaaba", "a********b") << endl;
  //cout << isMatch("aa","aa") << endl;
  //cout << isMatch("aaa","aa") << endl;
  //cout << isMatch("aa", "*") << endl;
  //cout << isMatch("aa", "a*") << endl;
  //cout << isMatch("ab", "?*") << endl;
  //cout << isMatch("aab", "c*a*b") << endl;
  return 0;
}
