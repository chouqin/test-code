#include <iostream>
#include <cctype>
#include <sstream>
#include <cstdio>
#include <map>

using namespace std;

enum Token {
  tok_eof,
  tok_let,
  tok_if,
  tok_true,
  tok_false,
  tok_add,
  tok_sub,
  tok_mul,
  tok_div,
  tok_int,
  tok_name,
  tok_left,
  tok_right,
};

string current_name = "";
int current_int = -1;
char current_char = ' ';
Token current_token = tok_eof;
stringstream cs;
// TODO: handle env in an stack way
map<string, int> env;

void gettok() {
  // skip whitespace
  if (isspace(current_char)) {
    while(cs.get(current_char) && isspace(current_char));
  }

  if (cs.eof()) {
    current_token = tok_eof;
    return;
  }

  switch(current_char) {
  case '+': cs.get(current_char); current_token = tok_add; return;
  case '-': cs.get(current_char); current_token = tok_sub; return;
  case '*': cs.get(current_char); current_token = tok_mul; return;
  case '/': cs.get(current_char); current_token = tok_div; return;
  case '(': cs.get(current_char); current_token = tok_left; return;
  case ')': cs.get(current_char); current_token = tok_right; return;
  }

  if (isdigit(current_char)) {
    cs.putback(current_char);
    cs >> current_int;
    cs.get(current_char);
    current_token = tok_int;
    return;
  }

  if (isalpha(current_char)) {
    current_name = current_char;
    while (cs.get(current_char) && isalnum(current_char)) current_name += current_char;
    if (current_name == "let") {
      current_token = tok_let;
      return;
    }
    if (current_name == "if") {
      current_token = tok_if;
      return;
    }
    if (current_name == "true") {
      current_token = tok_true;
      return;
    }
    if (current_name == "false") {
      current_token = tok_false;
      return;
    }
    current_token = tok_name;
  }
}

bool eval_bool() {
  switch(current_token) {
  case tok_true: gettok(); return true;
  case tok_false: gettok(); return false;
  default: printf("type missmatch, expect bool\n"); cout << current_token << endl; return false;
  }
}

int eval() {
  string name;
  int value;
  int left;
  int right;
  bool flag;
  switch (current_token) {
  case tok_left:
    gettok(); // eat '('
    value = eval();
    gettok(); // eat ')'
    return value;
  case tok_let:
    gettok(); // eat 'let'
    gettok(); // eat '('
    name = current_name;
    gettok(); // eat name
    value = eval();
    gettok(); // eat ')'
    env[name] = value;
    return eval();
  case tok_if:
    gettok(); // if
    flag = eval_bool();
    left = eval();
    right = eval();
    return flag ? left : right;
  case tok_name:
    name = current_name;
    gettok(); // name
    return env[name];
  case tok_add:
    printf("get add \n");
    gettok(); // +
    left = eval();
    right = eval();
    printf("+ %d %d\n", left, right);
    return left + right;
  case tok_sub:
    printf("get sub \n");
    gettok(); // -
    left = eval();
    right = eval();
    printf("- %d %d\n", left, right);
    return left - right;
  case tok_mul:
    printf("get mul \n");
    gettok(); // *
    left = eval();
    right = eval();
    printf("* %d %d\n", left, right);
    return left * right;
  case tok_div:
    gettok(); // /
    left = eval();
    right = eval();
    printf("/ %d %d\n", left, right);
    return left / right;
  case tok_int:
    value = current_int;
    gettok(); // int
    return value;
  }
}



void print_tokens() {
  gettok();
  while (current_token != tok_eof) {
    switch(current_token) {
    case tok_eof: cout << "end" << endl;break;
    case tok_let: cout << "let" << endl;break;
    case tok_if: cout << "if" << endl;break;
    case tok_true: cout << "true" << endl;break;
    case tok_false: cout << "false" << endl;break;
    case tok_add: cout << "add" << endl;break;
    case tok_sub: cout << "sub" << endl;break;
    case tok_mul: cout << "mul" << endl;break;
    case tok_div: cout << "div" << endl;break;
    case tok_int: cout << "int:" << current_int << endl;break;
    case tok_name: cout << "name:" << current_name << endl;break;
    case tok_left: cout << "(" << endl;break;
    case tok_right: cout << ")" << endl;break;
    }

    gettok();
  }
}

int main() {
  string line;
  getline(cin, line);
  cs.str(line);
  gettok();
  cout << eval() << endl;
  //print_tokens();
}
