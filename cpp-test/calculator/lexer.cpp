// 实现lexer
#include "cal.h"

Token_value curr_tok;
double value;
string name;

double error(const string &s) {
  cerr << "error: " << s << '\n';

  return 1;
}

Token_value get_token() {
  char ch;

  // 跳过空白字符
  do {
    if (!cin.get(ch)) return curr_tok = END;
  } while(ch != '\n' && isspace(ch));

  switch(ch) {
    case 0:
      return curr_tok = END;

    case ';':
    case '\n':
      return curr_tok = PRINT;

    case '+':
    case '-':
    case '*':
    case '/':
    case '(':
    case ')':
    case '=':
      return curr_tok = Token_value(ch);
  }

  // 如果是数字或者是.开头，说明是数字，
  // 直接利用cin读取，当然也可以读取整个字符串然后利用strtod来完成
  if (isdigit(ch) || ch == '.') {
    cin.putback(ch);
    cin >> value;
    return curr_tok = NUMBER;
  }

  // 如果是字母开头，说明类型是NAME, 读取整个字符串
  if (isalpha(ch)) {
    name = ch;
    while (cin.get(ch) && isalnum(ch)) name += ch;
    cin.putback(ch); // 注意这里的处理，否则会多消耗一个字符
    return curr_tok = NAME;
  }

  error("bad token");

  return curr_tok = PRINT;
}
