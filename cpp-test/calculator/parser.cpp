// 实现parser
#include "cal.h"

extern Token_value curr_tok;
extern double value;
extern string name;

map<string, double> table;

// 通过递归的方式实现
// get 代表是否要消耗掉当前的符号
double expr(bool get) {
  double left = term(get);

  while (curr_tok == PLUS ||
         curr_tok == MINUS) {
    if (curr_tok == PLUS) {
      left += term(true); // 消耗掉当前的+号
    } else {
      left -= term(true); // 消耗掉当前的-号
    }
  }

  return left;
}


double term(bool get) {
  double left = prim(get);

  while (curr_tok == MUL ||
         curr_tok == DIV) {
    if (curr_tok == MUL) {
      left *= prim(true); // 消耗掉当前的*号
    } else {
      double d = prim(true);
      if (d == 0) {
        return error("divide by 0");
      }
      left /= d; // 消耗掉当前的/号
    }
  }

  return left;
}

double prim(bool get) {
  if (get) get_token();

  switch(curr_tok) {
  case NUMBER:
    get_token();
    return value;

  case NAME: {
    double& v = table[name];
    get_token();
    if (curr_tok == ASSIGN) {
      v = expr(true);
    }
    return v;
  }

  case MINUS:
    return -1 * prim(true);

  case LP: {
    double inner = expr(true);

    // match ')'
    if (curr_tok != RP) {
      return error("( expected )");
    }

    get_token();

    return inner;
  }

  default: {
    cout << curr_tok;
    return error("primary expected");
  }
  }
}
