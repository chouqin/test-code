#include "cal.h"

// 测试文件

extern Token_value curr_tok;

int main() {
  while(cin) {
    get_token();

    if (curr_tok == END) break;
    if (curr_tok == PRINT) continue;

    cout << expr(false) << '\n';
  }

  return 0;
}
