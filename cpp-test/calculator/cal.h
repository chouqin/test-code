#ifndef CAL_H
#define CAL_H

// 头文件，定义数据结构，声明lexer和parser的接口

#include<string>
#include<iostream>
#include<map>
#include<cctype>

using namespace std;

enum Token_value {
  NAME, NUMBER, END,
  PLUS = '+', MINUS = '-', MUL = '*', DIV = '/',
  PRINT = ';', ASSIGN = '=', LP = '(', RP = ')'
};

Token_value get_token();

double expr(bool get);
double term(bool get);
double prim(bool get);

double error(const string& s);

#endif
