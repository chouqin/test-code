#include <iostream>
#include <cstdio>
#include <cstdlib>
using namespace std;

int partition(int *a, int low, int high) {
  int i = low - 1;
  int r = a[high];

  for (int j = low; j < high; ++j) {
    if (a[j] < r) {
      i += 1;

      // swap a[i] and a[j]
      int tmp = a[i];
      a[i] = a[j];
      a[j] = tmp;
    }
  }

  i += 1;
  // swap a[i] and a[high]
  a[high] = a[i];
  a[i] = r;

  cout << "after partition: ";
  for (int j = low; j <= high; ++j) {
    cout << a[j] << " ";
  }
  cout << "current i is " << i << endl;

  return i;
}

void quicksort(int * a, int low, int high) {
  if (low >= high) {
    return;
  }

  int mid = partition(a, low, high);
  quicksort(a, low, mid - 1);
  quicksort(a, mid + 1, high);
}


int main() {
  int N;
  cin >> N;
  for (int i = 0; i < N; ++i) {
    int n;
    cin >> n;
    int * a = new int[n];
    for (int j = 0; j < n; ++j) {
      cin >> a[j];
    }
    cout << "before sort: " << endl;
    for (int j = 0; j < n; ++j) {
      cout << a[j] << " ";
    }
    cout << endl;

    quicksort(a, 0, n-1);
    cout << "after sort: " << endl;
    for (int j = 0; j < n; ++j) {
      cout << a[j] << " ";
    }
    cout << endl;
  }

  return 0;
}
