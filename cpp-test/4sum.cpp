#include <iostream>
#include <map>
#include <vector>
#include <algorithm>
#include <set>
#include <utility>

using namespace std;

struct pair_compare {
  bool operator() (const pair<int, int> &a, const pair<int, int> &b) {
    if (a.first == b.first) {
      return a.second > b.second;
    } else {
      return a.first > b.first;
    }
  }
};

typedef set<pair<int, int>, pair_compare> pair_set;

vector<int> get_vector_int(const pair<int, int> & a, const pair<int, int> &b) {
  vector<int> result;
  result.push_back(a.first);
  result.push_back(a.second);
  result.push_back(b.first);
  result.push_back(b.second);
  sort(result.begin(), result.end(), [](int a, int b) {
       return a < b;
  });
  return result;
}

map<int, int> get_value_count(vector<int> values) {
  map<int, int> count;
  for (auto v: values) {
    if (count.count(v) == 0) {
      count[v] = 1;
    } else {
      count[v] = count[v] + 1;
    }
  }
  return count;
}

vector <vector<int> > foursum(vector<int> &num, int target) {
  map<int, pair_set> twosums;
  map<int, int> valuecount = get_value_count(num);
  for (size_t i = 0; i < num.size(); ++i) {
    for (size_t j = 0; j < num.size(); ++j) {
      int a = num[i], b = num[j];
      int sum = a + b;

      if (twosums.count(sum) == 0) {
        pair_set s;
        if (a > b) {
          s.insert(make_pair(b, a));
        } else {
          s.insert(make_pair(a, b));
        }
        twosums.insert(make_pair(sum, s));
      } else {
        if (a > b) {
          twosums.find(sum)->second.insert(make_pair(b, a));
        } else {
          twosums.find(sum)->second.insert(make_pair(a, b));
        }
      }
    }
  }

  //for (auto twosum: twosums) {
    //cout << twosum.first << ": ";
    //for (auto p: twosum.second) {
      //cout << "<" << p.first << ", " << p.second << ">, ";
    //}
    //cout << endl;
  //}

  set<int> visited;
  set<vector<int> >result;
  for (auto twosum: twosums) {
    int sum = twosum.first;
    int pair_sum = target-sum;
    if (visited.count(sum) != 0) {
      continue;
    }
    if (twosums.count(pair_sum) == 0) {
      continue;
    }
    visited.insert(sum);
    visited.insert(pair_sum);
    //printf("visit %d and %d\n", sum, pair_sum);

    pair_set s1 = twosum.second;
    pair_set s2 = twosums[pair_sum];

    for (auto p1: s1) {
      for (auto p2: s2) {
        auto candidate = get_vector_int(p1, p2);
        auto count = get_value_count(candidate);
        bool valid = true;
        //printf("candidate: <%d, %d>, <%d, %d>\n", p1.first, p1.second, p2.first, p2.second);
        for (auto v: count) {
          if (valuecount[v.first] < v.second) {
            valid = false;
            break;
          }
        }
        if (valid) {
          //printf("insert pairs: <%d, %d>, <%d, %d>\n", p1.first, p1.second, p2.first, p2.second);
          result.insert(get_vector_int(p1, p2));
        }
      }
    }
  }

  vector<vector<int> > v(result.begin(), result.end());
  return v;
}


int main() {
  vector<int> num = {91277418,66271374,38763793,4092006,11415077,60468277,1122637,72398035,-62267800,22082642,60359529,-16540633,92671879,-64462734,-55855043,-40899846,88007957,-57387813,-49552230,-96789394,18318594,-3246760,-44346548,-21370279,42493875,25185969,83216261,-70078020,-53687927,-76072023,-65863359,-61708176,-29175835,85675811,-80575807,-92211746,44755622,-23368379,23619674,-749263,-40707953,-68966953,72694581,-52328726,-78618474,40958224,-2921736,-55902268,-74278762,63342010,29076029,58781716,56045007,-67966567,-79405127,-45778231,-47167435,1586413,-58822903,-51277270,87348634,-86955956,-47418266,74884315,-36952674,-29067969,-98812826,-44893101,-22516153,-34522513,34091871,-79583480,47562301,6154068,87601405,-48859327,-2183204,17736781,31189878,-23814871,-35880166,39204002,93248899,-42067196,-49473145,-75235452,-61923200,64824322,-88505198,20903451,-80926102,56089387,-58094433,37743524,-71480010,-14975982,19473982,47085913,-90793462,-33520678,70775566,-76347995,-16091435,94700640,17183454,85735982,90399615,-86251609,-68167910,-95327478,90586275,-99524469,16999817,27815883,-88279865,53092631,75125438,44270568,-23129316,-846252,-59608044,90938699,80923976,3534451,6218186,41256179,-9165388,-11897463,92423776,-38991231,-6082654,92275443,74040861,77457712,-80549965,-42515693,69918944,-95198414,15677446,-52451179,-50111167,-23732840,39520751,-90474508,-27860023,65164540,26582346,-20183515,99018741,-2826130,-28461563,-24759460,-83828963,-1739800,71207113,26434787,52931083,-33111208,38314304,-29429107,-5567826,-5149750,9582750,85289753,75490866,-93202942,-85974081,7365682,-42953023,21825824,68329208,-87994788,3460985,18744871,-49724457,-12982362,-47800372,39958829,-95981751,-71017359,-18397211,27941418,-34699076,74174334,96928957,44328607,49293516,-39034828,5945763,-47046163,10986423,63478877,30677010,-21202664,-86235407,3164123,8956697,-9003909,-18929014,-73824245};
  int target = -236727523;

  for (auto v: foursum(num, target)) {
    for (auto i: v) {
      cout << i << " ";
    }
    cout << endl;
  }

  return 0;
}
