# -*- coding=utf-8 -*-

#import datetime
#def convert_date(date_string):
    #d = datetime.datetime.strptime(date_string, "%Y-%m-%d")
    #changed = d + datetime.timedelta(days=25)
    #return changed.strftime("%Y-%m-%d")

#print convert_date("2014-09-25")


#def get_word_count(li):
    #di = {}
    #for s in li:
        #if s in di:
            #di[s] += 1
        #else:
            #di[s] = 1

    #return [(k, v) for k, v in di.iteritems()]

#from collections import defaultdict
#def get_word_count2(li):
    #di = defaultdict(lambda : 0)
    #for s in li:
        #di[s] += 1
    #return [(k, v) for k, v in di.iteritems()]

#li = ["a", "ab", "c", "a", "b", "c"]
#print get_word_count2(li)

#class Basic:
    #def __init__(self, a, b):
        #self.a = a
        #self.b = b

    #def sum(self):
        #return self.a + b


#i = Basic(1, 2)
#print i.a, i.b, i.sum()

#from collections import namedtuple
#Basic = namedtuple("Basic", ["a", "b"])
#i = Basic(1, 2)
#print i.a, i.b


#def count_times(func):
    #def wrapper(*arg, **kwargs):
        #wrapper.called += 1
        #print "func get called, current count is %d" % wrapper.called
        #func(*arg, **kwargs)
    #wrapper.called = 0

    #return wrapper


#@count_times
#def greet(who):
    #print "Hello, %s" % who

#greet("Person1")
#greet("Person2")

def foo(a, b):
    print a, b

args = (1, 2)
foo(*args)

#def foo(*args):
    #print args[0], args[1]

#args = [1, 2]
#foo(*args)

#foo(1, 2)

#def bar(**kwargs):
    #print kwargs["a"], kwargs["b"]

#bar(1, 2)
#bar(a = 1, b = 2)

#di = {"a": 1, "b": 2}
#bar(**di)

#def foobar(*args, **kwargs):
    #print args
    #print kwargs

#foobar(1, 2, a=1)

#def foobar(a, b, *args, **kwargs):
    #print a, b
    #print args
    #print kwargs

#foobar(1, 2, 3, 4, c=5, d=6)
