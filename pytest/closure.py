# -*- coding=utf-8 -*-

def create_multipliers():
    return [lambda x : i * x for i in range(5)]

def create_multipliers_new():
    def get_multiplier(i):
        return lambda x: i * x
    return [get_multiplier(i) for i in range(5)]

for multiplier in create_multipliers():
    print multiplier(2)

for multiplier in create_multipliers_new():
    print multiplier(2)
