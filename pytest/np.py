# -*- coding=utf-8 -*-


import numpy as np


def cov1(A, coes):
    cov = np.zeros(A.shape)
    for i in range(len(coes)):
        cov += coes[i] * np.outer(A[i, :], A[i, :])

    print cov

def cov2(A, coes):
    cov = np.dot(coes * A.T, A)
    print cov


if __name__ == '__main__':

    A = np.array([[1, 2], [3, 4]])
    coes = np.array([7, 8]).T
    cov1(A, coes)
    cov2(A, coes)
