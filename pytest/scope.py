# -*- coding=utf-8 -*-

def scope_test1():
    #y = [1, 2, 3, 4]
    y = []
    to_find = 2
    for idx, value in enumerate(y):
        if value == to_find:
            break

    print idx

li = []
def scope_test2():
    #li += [1] # 这一行等价于li = li + [1]，会报错
    li.extend([1])


#scope_test1()
scope_test2()

