# -*- coding=utf-8 -*-


from pyquery import PyQuery as pq
import csv
import urllib2
import urllib


def grade1():
    d = pq(filename='/home/chouqin/Desktop/grading/table.html')
    trs = d("tr")

    with open("/home/chouqin/Desktop/grading/1.csv", "w") as f:
        writer = csv.writer(f, delimiter=',')
        for i in range(1, len(trs)):
            tr = trs.eq(i)
            tds = tr.find("td")
            print len(tds)
            name = tds.eq(1).children().eq(0).val()
            weight = tds.eq(2).children().eq(0).val()
            grade = tds.eq(3).children().eq(0).val()
            print name, weight, grade
            writer.writerow([name, weight, grade])

def get_english_name(cname):
    data = {
        '__VIEWSTATE':"/wEPDwULLTEyMzcwNzIwODEPZBYCAgMPZBYEAgcPPCsADQEADxYEHgtfIURhdGFCb3VuZGceC18hSXRlbUNvdW50AgFkFgJmD2QWBgIBD2QWBmYPZBYCZg8VAQExZAIBDw8WAh4EVGV4dAU65aSn5a2m54mp55CG5a6e6aqM77yIMe+8iSAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRkAgIPDxYCHwIFDVBoeXNpY3MgTGFiIElkZAICDw8WAh4HVmlzaWJsZWhkZAIDDw8WAh8DaGRkAgkPDxYCHwIFNuWFseafpeivouWIsOOAkDHjgJHmnaHorrDlvZUs5b2T5YmN6aG16Z2i56ys44CQMeOAkemhtWRkGAEFAkd2DzwrAAoBCAIBZD3FRwFdJXIkSRzUBC26XcXjy0Xl",
        '__VIEWSTATEGENERATOR':"201BE16D",
        '__EVENTVALIDATION':"/wEWAwLy2MO8CwL81vvvDQLdmpmPAZlCAnLeucgeNGxOenUJaYXaJH/A",
        'txtKcmc': cname.encode("gb2312"),
        'BtnOk': u"查询".encode("gb2312")
    }
    url = 'http://202.120.35.10:8080/ac/KCQuery/kcquery.aspx'
    response = urllib2.urlopen(url, urllib.urlencode(data))
    html = response.read().decode("gb2312")
    #print html
    d = pq(html)
    tr = d("table#Gv tr").eq(1)
    ename = tr.children().eq(2).text()
    print cname, ename
    return ename.encode("utf-8")


def grade2():
    with open("/home/chouqin/Desktop/grading/2.csv", "w") as f:
        writer = csv.writer(f, delimiter=',')
        with open("/home/chouqin/Desktop/grading/chengjidan.csv") as f1:
            reader = csv.reader(f1)
            for row in reader:
                year = row[0]
                s = row[1]
                cname = row[3].decode("gb2312")
                code = row[4]
                grade = row[5]
                weight = row[6]
                ename = get_english_name(cname)
                writer.writerow([year, s, code, grade, weight, ename])

if __name__ == "__main__":
    grade2()
