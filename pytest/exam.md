# Python测试题

## 题目1: 

给定一个字符串，判断它是否为一个以.com或.org结尾的email。

### 答案

```python
import re

address = re.compile('[\w\d.+-]+@([\w\d.]+\.)+(com|org)', re.UNICODE)

candidates = [
    u'first.last@example.com',
    u'first.last+category@gmail.com',
    u'valid-address@mail.123example.org',
    u'not-valid@example.foo',
    ]

for candidate in candidates:
    print
    print 'Candidate:', candidate
    match = address.search(candidate)
    if match:
        print '  Matches'
    else:
        print '  No match'
```

### 考察

考察python中正则表达式的基本用法。

## 题目2

写一个函数，输入一个"%Y-%m-%d"格式的日期（如"2014-09-25"），
返回这个日期后25天的日期，用"%Y-%m-%d"的字符串表示。
比如输入"2014-09-25"，返回"2014-10-20"。

### 答案

```python
import datetime
def convert_date(date_string):
    d = datetime.datetime.strptime(date_string, "%Y-%m-%d")
    changed = d + datetime.timedelta(days=25)
    return changed.strftime("%Y-%m-%d")
```

### 考察

考察Python标准库的`datetime`模块。

## 题目3：Closure

下面代码的输出是0, 2, 4, 6, 8吗？ 如果不是，
应该怎么修改程序让它输出0, 2, 4, 6, 8。

```python
def create_multipliers():
    return [lambda x : i * x for i in range(5)]

for multiplier in create_multipliers():
    print multiplier(2)

```

### 答案
```python
def create_multipliers_new():
    def get_multiplier(i):
        return lambda x: i * x
    return [get_multiplier(i) for i in range(5)]

for multiplier in create_multipliers_new():
    print multiplier(2)
```

### 考察点

Python中的闭包是延迟绑定的，当一个值被使用的时候才去它的scope找它的值。

这道题比较难。

## 题目4：

`li`是一个`list`，如何获取li中最大元素的索引？


### 答案 

```python
import operator
max_index, max_value = max(enumerate(li), key=operator.itemgetter(1))
```

### 考察点

考察enumerate，以及operator.itemgetter。

## 题目5：

请用`lambda`实现题目4中的`operator.itemgetter`。

### 答案

```python
max_index, max_value = max(enumerate(li), key=lambda t: t[0])
```

### 考察点

考察`lambda`的用法

## 题目6

`li`是一个整数的`list`，返回一个`list`，它的元素是`li`中所有的偶数加1

## 答案

```python
return [i + 1 for i in li if i % 2 == 0]
```

## 考察点

考察List Comprehension，这是Pythonic的写法。

## 题目7

在Python 2中`range`和`xrange`有什么区别？
如果要从一个文件中读取一系列的元素用于遍历，
要达到类似`xrange`的效果，应该怎么做？

### 答案

`range`会在内存中生成一个list，而`xrange`只是一个generator，
它的元素是惰性求职的，而且使用过之后就不存在了。

可以使用`yield`来实现generator:

```python
def generate():
  with open(filename) as f:
    for line in f:
      yield line

for line in generate():
  print line
```

### 考察点

考察`xrange`和`range`的区别，以及`yield`用于实现generator。
同时，考察如何读取文件。

## 题目8

s是字符串"a,b,c,d,e,f"，把s的","换成";"。

### 答案

```python
";".join(s.split(","))
```

### 考察点

考察字符串的基本操作，`join`和`split`的用法。

## 题目9

假设`li`是一个list，长度大于3，把li的除去第一个和最后一个元素的中间元素反转。
比如`li`是[1, 2, 3, 4, 5, 6]，返回[1, 5, 4, 3, 2, 6]

### 答案

```python
def partial_reverse(li):
  mid = li[1:-1]
  mid.reverse()
  return li[:1] + mid + li[-1:]
```

### 考察点

考察list的`slice`，`reverse`和拼接操作。

## 题目10

`li`是一个字符串列表，使用python的`dict`统计其中每一个单词的次数。
比如`li`是`["a", "ab", "c", "a", "b", "c"]`
返回`[("a", 2), ("ab", 1), ("c", 2), ("b", 1)]`（顺序没有关系）。

### 答案

```python
def get_word_count(li):
    di = {}
    for s in li:
        if s in di:
            di[s] += 1
        else:
            di[s] = 1

    return [(k, v) for k, v in di.iteritems()]
```

### 考察点

考察Python中的dict的用法（查找，添加，修改，遍历）

## 题目11

使用Python的`collections.defaultdict`完成上面的功能。

### 答案

```python
from collections import defaultdict
def get_word_count(li):
    di = defaultdict(lambda : 0)
    for s in li:
        di[s] += 1
    return [(k, v) for k, v in di.iteritems()]
```

### 考察点

考察`defaultdict`的用法

## 题目12

如何读取csv文件中的每一行，一行的每一个元素如何得到？

### 答案

```python
import csv
with open(filename) as f:
  reader = csv.reader(f)
  for row in reader:
    print row # row是一个list，row[0]表示第一列的元素
```

### 考察点

查看Python读取csv。

### 题目13

使用Python的`class`定义一个类，它具有两个int的属性，`a`和`b`,
有一个`sum`方法，得到`a`和`b`的和。
初始化这个类的一个示例，并打印出它的两个属性和`sum`方法。

### 答案

```python
class Basic:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def sum(self):
        return self.a + self.b


i = Basic(1, 2)
print i.a, i.b, i.sum()
```

### 考察点

考察Python基础的面向对象编程。

## 题目14

在上题中，如果不需要`sum`函数，只需要定义一个有属性`a`和`b`的结构体，
请使用`namedtuple`实现。

### 答案

```python
from collections import namedtuple
Basic = namedtuple("Basic", ["a", "b"])
i = Basic(1, 2)
print i.a, i.b
```

### 考察点

考察`namedtuple`的用法。

### 题目16

下面这段代码的输出是什么？

```python
def append_to(element, to=[]):
    to.append(element)
    return to

list1 = append_to(12)
print list1

list2 = append_to(42)
print list2
```

请解释一下原因？

### 答案

`list1`是`[12]`，`list2`是`[12, 42]`。
因为在Python中的默认参数只会被求值一次，
在一开始给`to`赋值为`[]`，后面的两次操作会改变这个list。

这是一个Python的很常见的陷阱，建议是不要使用可变的值作为函数的默认参数。
比如上面的函数，推荐的写法是这么写：

```python
def append_to(element, to=None):
    if to is None:
        to = []
    to.append(element)
    return to
```

### 考察点

考察默认参数以及可变默认参数的陷阱。


## 题目17

下面的各段代码运行会报错吗，如果不会，输出的结果是什么？

#### 代码17-1

```python
def foo(a, b):
    print a, b

args = [1, 2]
foo(*args)
```

答案：运行结果`1, 2`。

可以通过`*`把一个list或者tuple转换为参数列表传给函数。

#### 代码17-2

```python
def foo(*args):
    print args[0], args[1]

foo(1, 2)
```

答案：运行结果`1, 2`。

在函数定义时，可以使用`*`声明函数可以接受可变长度的参数。
得到`args`是一个`tuple`。

#### 代码17-3

```python
def foo(*args):
    print args[0], args[1]

args = [1, 2]
foo(*args)
```

答案：运行结果`1, 2`，和17-1同理

#### 代码17-4

```python
def bar(**kwargs):
    print kwargs["a"], kwargs["b"]

bar(a = 1, b = 2)

di = {"a": 1, "b": 2}
bar(**di)
```

答案：运行结果打印两次`1, 2`。

道理同17-2和17-3，只是把tuple改为dict。
可以使用`**`来定义可变的传名参数，
也可以使用`**`将一个dict转换为可变参数参数列表传给函数

#### 代码17-5

```python
def bar(**kwargs):
    print kwargs["a"], kwargs["b"]

bar(1, 2)
```

答案：运行报错，
不能使用不传名的参数列表调用只接收传名参数的函数，
反之亦然（不能使用`foo(a=1, b=2)`来调用`foo(*args)`）。


#### 代码17-5

```python
def foobar(*args, **kwargs):
    print args
    print kwargs

foobar(1, 2, a=1)
```

答案：

```
(1, 2)
{'a': 1}
```

当同时定义了可变参数和传名参数时，
函数调用的非传名参数给`args`, 传名参数给`kwargs`。

#### 代码17-6

```python
def foobar(*args, **kwargs):
    print args
    print kwargs

foobar(a=1, 1, 2)
```

答案：运行会报错，
函数调用时传名参数必须在非传名参数之后，
在函数声明时也是（`**kwargs`必须在`*args`之后）。

#### 代码17-7

```python
def foobar(a, b, *args, **kwargs):
    print a, b
    print args
    print kwargs

foobar(1, 2, 3, 4, c=5, d=6)
```

运行结果：

```
1 2
(3, 4)
{'c': 5, 'd': 6}
```

在函数调用时，按顺序把参数列表中的值赋给非可变参数（上面的`a`和`b`），
然后把剩下的非传名参数赋给`args`，传名参数赋给`kwargs`。

#### 代码17-8

```python
def foobar(a, b, *args, **kwargs):
    print a, b
    print args
    print kwargs

foobar(1, 2, 3, 4, a=5, d=6)
```

运行会报错，因为a被赋了两个值。

### 考察点

考察函数的可变参数。


## 题目18

使用python的装饰器封装一个函数，统计这个函数的调用次数。

### 答案

```python
def count_times(func):
    def wrapper(*arg, **kwargs):
        wrapper.called += 1
        print "func get called, current count is %d" % wrapper.called
        func(*arg, **kwargs)
    wrapper.called = 0

    return wrapper

@count_times
def greet(who):
    print "Hello, %s" % who

greet("Person1")
greet("Person2")
```

### 考察点

考察装饰器的用法。


## 题目19

使用`numpy`，对一个5 x 5的随机矩阵进行Normalize(所有值转换为0到1的范围):

### 答案

```python
import numpy as np
Z = np.random.random((5,5))
Zmax,Zmin = Z.max(), Z.min()
Z = (Z - Zmin)/(Zmax - Zmin)
print Z
```

### 考察点

考察numpy的用法。


## 题目20

假设y是一个list，下面的程序运行会出现Exception吗？

```python
for idx, value in enumerate(y):
    if value == to_find:
        break
    
print idx
```

### 答案

如果y是一个empty的list，会抛Exception，
否则不会。

### 考察点

考察python的scope，一个变量的scope要比在java中更长。

这样的写法很容易出错，建议的写法是先初始化一个值，
在找到合适的值之后再把值赋给它。

```
result = -1
  for idx, value in enumerate(y):
      if value == to_find:
          result = idx
          break
```

最后使用result。

## 附加题

有没有在Python的解释器中运行过`import this`？
请谈谈对于`The Zen of Python`的理解。


## 参考资料

1. Python指南: http://docs.python-guide.org/en/latest/
2. Numpy测试：http://www.labri.fr/perso/nrougier/teaching/numpy.100/
3. A Few of My Favorite Python Things: http://www.infoq.com/presentations/A-Few-of-My-Favorite-Python-Things
4. Python Tricks: http://sahandsaba.com/thirty-python-language-features-and-tricks-you-may-not-know.html
5. Python高级特性：http://nbviewer.ipython.org/github/rasbt/python_reference/blob/master/tutorials/not_so_obvious_python_stuff.ipynb
