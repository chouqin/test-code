/*
 * Implementation of Mini Regular Expression
 * Support wildcard: ^(start), $(end), .(any character), *(repeat zero or more times)
 */


#include <stdio.h>

/*
 * 从s位置开始match正则表达式r
 * 注意和match的区别，在match中，其实位置可以有任意的偏移
 */
int match_start(const char* r, const char* s);

int match(const char* r, const char* s) {
  if (*r == '^') {
    return match_start(r++, s);
  }

  /*
   * 如果第一个字符不是^，那么对于每种偏移位置都需要考虑
   */
  do {
    if (match_start(r, s)) return 1;
  } while (*s++ != '\0');
  /* 注意这里不能用
   *
   * while (*s++) {
   * ...
   * }
   *
   * 因为如果一开始s就是空白，还是可以match住空白的正则表达式
   * */

  return 0;
}

int match_start(const char* r, const char* s) {
  char ch;

  if (!(*r)) {
    return 1;
  }

  if (*r == '$') {
    return *s == '\0';
  }

  /*
   * 对于r+1是*号的情况，需要特殊处理，此时r所指的字符可以在当前字符串中
   * 出现0次或多次，每种情况都需要考虑
   */
  if (*(r+1) == '*') {
    do {
      if (match_start(r+2, s)) return 1;
    } while (*s != '\0' && (*r == *s++ || *r == '.')); /* 注意检查边界条件 */
    return 0;
  }

  return *s != '\0' &&
    (*r == *s || *r == '.') &&
    match_start(r + 1, s + 1);
}

int main(int argc, char** argv) {
  if (match(argv[1], argv[2])) {
    printf("%s match for regular expression %s\n", argv[2], argv[1]);
  } else {
    printf("%s doesn't match for regular expression %s\n", argv[2], argv[1]);
  }

  return 0;
}
