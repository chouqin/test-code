#include <stdio.h>

/*
 * 输出5, 地址相加的大小为sizeof(a);
 * */

int main() {
  int a[5] = {1, 2, 3, 4, 5};
  int *p = (int *)(&a + 1);
  /*int *p = &a + 1; // 这样会有warning*/
  printf("%d\n", *(p-1));
  return 0;
}
