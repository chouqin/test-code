#include "b.h"

#include <stdio.h>

extern int a; // 只有在使用extern且没有初始化时才不会报错

int main(int argc, char** argv) {
  int i;

  i = get_int();

  printf("%d %d\n", i, a);

  return 0;
}

