#include <stdio.h>

int main()
{
    int a = 10;

    int b = a ?: 6; // 10, 这个表达式的效果在Python里面经常用,
    // 不过在C程序里面一般不这么写吧

    printf("%d\n", b);

    return 0;
}
