#include <stdio.h>
#include <string.h>

struct tab
{
    int id;
    float amount1;
    double amount;
    char name[30];
    char *description;
};

struct sdshdr {
    long len;
    long free;
    char buf[]; // 相当于这个没有占空间
};

union data {
    int i;
    float f;
    char str[20];
};

// 测试对齐
struct align {
char c;
int i;
char b;
};

// 传进来跟str[]和一个指针没什么区别，
// 推荐还是直接用指针
// 返回的是指针的大小
int get_strlen(char str[]) { // 这里不能把char* 改为char[]
  return sizeof(str);
}

int main()
{
    struct tab t;

    printf("%d %d %d %d %d\n",
            sizeof(t.id),
            sizeof(t.amount1),
            sizeof(t.name),
            sizeof(t.description),
            sizeof(t));
    /* 4 4 30 8 56*/

    printf("%d\n", sizeof(struct sdshdr));
    printf("%d\n", sizeof(union data));


    /*char * str;*/
    /*printf("%d\n", sizeof(str));*/

    /*str = "hello world!";*/
    /*printf("%d\n", sizeof(str));*/

    /*char str[20];*/
    /*printf("%d\n", sizeof(str));*/
    /*printf("%s\n", str); [>gabage value<]*/

    /*str = "hello world!";*/
    /*printf("%d\n", sizeof(str));*/

    /* 测试中文 */
    char *str = "中文中文";
    printf("%s %d %d\n", str, strlen(str), sizeof(str)); /*结果：中文中文*/

    char str1[20] = "Hello World!";
    printf("%s %d %d\n", str1, strlen(str1), get_strlen(str1)); /*结果：Hello world 12 8*/
    str = str1;
    str[0] = 'h';
    printf("%s %d %d\n", str, strlen(str), sizeof(str)); /*结果：hello world 12 8*/

    // 通过指针指向一个data area，和申请一个array，然后以数据初始化不一样
    // 后者是可以修改的。
    /*char *pmessage = "now is the time"; [> a pointer <]*/
    /*pmessage[0] = 'N'; [> segment fault <]*/

    // 测试通过new的指针，得到的还是一个指针，而不是一个array
    str = (char *)malloc(2);
    str[0] = '1';
    str[1] = '2';
    printf("%s %d %d\n", str, strlen(str), sizeof(str)); /* 12 2 8 */
    free(str);

    unsigned int a;
    short b;
    long c;
    printf("%d %d %d\n", sizeof(a), sizeof(b), sizeof(c));
    /* 4 2 4*/


    struct align al;
    printf("after align: %d\n", sizeof(al));

    return 0;
}
