fold1 :: (a -> b -> a) -> a -> [b] -> a

fold1 f = flip $ foldr (\a b g -> b (f g a)) id  

-- D
{-fold1 = foldr . flip-}
