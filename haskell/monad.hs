sequence' :: Monad m => [m a] -> m [a]
sequence' [] = return []
sequence' (m: ms) 
  = do a <- m
       as <- sequence' ms
       return (a : as)

{-sequence' ms = foldr func (return []) ms-}
  {-where-}
    {-func :: (Monad m) => m a -> m [a] -> m [a]-}
    {-func m acc = m : acc-}

mapM' :: Monad m => (a -> m b) -> [a] -> m [b]
{-mapM' f as = sequence' (map f as)-}

mapM' f [] = return []
{-mapM' f (a : as)-}
  {-= f a >>= \b -> mapM' f as >>= \ bs -> return (b : bs)-}

mapM' f (a : as) = 
  do 
    b <- f a 
    bs <- mapM' f as
    return (b : bs)

filterM' :: Monad m => (a -> m Bool) -> [a] -> m [a]

filterM' _ [] = return []
filterM' p (x : xs) = 
  do 
    flag <- p x
    ys <- filterM' p xs
    if flag then return (x : ys) else return ys

{-下面的不能编译通过，p x的类型不是m Bool-}
{-filterM' p (x : xs) = -}
  {-do -}
    {-ys <- filterM' p xs-}
    {-if p x then return (x : ys) else return ys-}

foldLeftM :: Monad m => (a -> b -> m a) -> a -> [b] -> m a
foldLeftM f a [] = return a
foldLeftM f a (x:xs) = f a x >>= \ fa ->
  foldLeftM f fa xs 

foldRightM :: Monad m => (a -> b -> m b) -> b -> [a] -> m b
foldRightM f b [] = return b
foldRightM f b (x:xs) = foldRightM f b xs >>= \ fb ->
  f x fb


liftM1 :: Monad m => (a -> b) -> m a -> m b
{-liftM f m = -}
  {-do-}
    {-x <- m-}
    {-return (f x)-}
{-liftM1 f m = m >>= \ a -> m >>= \ b -> return (f a)-}
liftM1 f m = m >>= \ a -> m >>= \ b -> return (f b)
