fibs :: [Integer]
fibs = 0 : 1 : [x + y | (x, y) <- zip fibs (tail fibs)]

data Tree a = Leaf
            | Node (Tree a) a (Tree a)

{-repeatTree :: a -> Tree a-}


-- test order, order matters
-- when compile this function, will get a wranning
{-isZero :: Int -> Bool-}
{-isZero n = False-}
{-isZero 0 = True-}

last :: [a] -> a
last [x] = x
last (_ : xs) = last xs
