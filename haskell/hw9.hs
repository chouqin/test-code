import Data.List
import Data.Char
import Unsafe.Coerce

data Nat = Zero
  | Succ Nat
  deriving Show

natToInteger :: Nat -> Integer
{-natToInteger = \n -> genericLength [c | c <- show n, c == 'S']-}
{-natToInteger = \n -> length [c | c <- show n, c == 'S']-}

natToInteger (Succ n) = 1 + natToInteger n
natToInteger Zero = 0

integerToNat :: Integer -> Nat
{-integerToNat n-}
  {-= product [(unsafeCoerce c) :: Integer | c <- show n]-}

integerToNat (n+1) = let m = integerToNat n in Succ m
integerToNat 0 = Zero


data Tree = Leaf Integer | Node Tree Integer Tree

occurs :: Integer -> Tree -> Bool
occurs m n = m == n
occurs m (Node l n r)
  | m == n = True
  | m < n = occurs m l
  | otherwise = occurs m r
