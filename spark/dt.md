# 决策树简介及其在Spark中的实现

## 决策树简介

### 构建决策树

### 特征划分

### 最好的分割

## 在Spark中的实现

### 划分特征

findSplitBins: 对每一个feature进行划分，得到的结果是(numFeature，numBin)

### 逐层计算

#### 第一步

对于每一个labeledpoint，计算它应该被放入哪个桶

findBin: 找到一个labeledpoint应该放入featureIndex的哪一个bin

findBinsForLevel: 对于每一个节点，找到这个labeledpoint应该放入featureindex的哪一个bin
为什么每一个节点会不一样，因为有可能这个labelpoint不属于这个节点。
但是实现的时候可以做改进，甚至可以节省很多的空间。

#### 第二步

更新每一个桶的统计信息。

binMappedRDD.aggregate(Array.fill[Double](binAggregateLength)(0))(binSeqOp,binCombOp)

统计信息的格式：

nodeIndex -> featureIndex -> binIndex -> labelIndex

#### 第三步

由桶的信息得到每一个分割的统计信息。

getBinDataForNode: 获取node的binData。
extractLeftRightNodeAggregates

leftNodeAgg
rightNodeAgg

#### 第三步

计算每一个分割的信息增量，找到信息增量最大的分割。

calculateGainForSplit: 通过splitIndex分割featureIndex，
利用得到的leftnodeAgg和rightnodeagg计算信息增量。
同时计算这次分割会导致多大的信息增量，以及predict是什么。
总体是在InformationGainStats。

遍历获取能够给出最大信息增量的(featureIndex, splitIndex)

### 进阶：非连续性的Feature

### 扩展：一个诡异的bug
