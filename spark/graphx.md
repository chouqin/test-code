# 概述

GraphX是一个新的（alpha版本）用于并行图计算的Spark API。
在更高的层次上，GraphX扩展了Spark的RDD，引入了弹性分布式属性图
（Resilient Distributed Property Graph）：一种节点和边都具有属性的有向图。
为了支持图计算，GraphX提供了一系列的基础的运算，
（比如`subgraph`, `joinVertices`和`mapReduceTriplets`）和一个优化过的Pregel API的变体。
同时，为了简化图分析任务，
GraphX包含了很多了的图算法（如PageRank）和用于构建图的工具方法，
而且不断有新的算法加入进来。

**GraphX 现在是一个alpha模块，尽管我们尽量减少API的改动，
一些API仍然可能在将来的版本中发生改变。**

## 并行图计算背景介绍

从社交网络到语言建模，图数据的规模和重要性都在不断增长，
推动了很多*图并行*系统的发展（比如[Giraph](http://giraph.apache.org)和
[GraphLab](http://graphlab.org)）。
通过限制他们能够表达的计算类型和引入新的划分和分布图的技术，
这些系统能够有效的执行图计算算法，
比通用的*数据并行*系统快几个数量级。

![数据并行 VS 图并行](http://spark.apache.org/docs/latest/img/data_parallel_vs_graph_parallel.png)

然而，给这些系统带来实质性性能增长的限制同样会使得表达很多图分析其他过程很难用这些系统来表达：
比如说构建图，修改图的结构或者跨越多个图的计算。
而且，如何看待数据取决于我们的目的，
同样的原始数据可能把它看成不同的表或者图（具有不同的视图）。

![表和图](http://spark.apache.org/docs/latest/img/tables_and_graphs.png)

因此，为了利用不同视图的特性来简易和有效地表达计算，
在不同的图和表视图之中移动同一份物理数据很有必要。
然而，现有的分析流水线必须把图并行系统和数据并行系统组合起来，
导致很大的数据移动和复制，模型的复杂度也很高。

![图分析流水线](http://spark.apache.org/docs/latest/img/graph_analytics_pipeline.png)

GraphX的目标就是统一图并行和数据并行到同一个系统，提供一个组合的API。
GraphX的API使用户能够把数据看成图和集合（RDD），不需要数据的移动和复制。
通过吸收目前图并行系统的最新成果，GraphX能够优化图运算的执行。

## GraphX代替Spark Bagel API

在GraphX发布之前，Spark中的图计算使用的是Bagel模块（一个Pregel的实现）。
Graphx提供了一个更加丰富的属性图API，一个改进的Pregel抽象版本，
同时通过系统优化来改善性能和减少内存开销，从而改进了Bagel。
尽管我们计划最终废弃Bagel，
我们会继续支持[Bagel API](http://spark.apache.org/docs/latest/api/scala/index.html#org.apache.spark.bagel.package)和
[Bagel编程指南](http://spark.apache.org/docs/latest/bagel-programming-guide.html)。
然而，我们鼓励使用Bagel的用户尝试新的GraphX的API，
同时指出那些让从Bagel迁移到GraphX变得复杂的问题。

## 从0.9.1迁移

Spark 1.0中的GraphX相对于Spark 0.9.1 包含了一个用户需要面对的接口改变，
`EdgeRDD`现在会保存邻接节点的属性来构成三元组，
因此它包含了一个新增的类型参数。
类型为`Graph[VD, ED]`的图的边的类型为`EdgeRDD[ED, VD]`而不是`EdgeRDD[ED]`。

# 开始使用GraphX


要开始使用GraphX，首先需要import Spark和GraphX到你的项目，如下所示：

```scala
import org.apache.spark._
import org.apache.spark.graphx._
// 为了让一些例子运行我们还需要import RDD
import org.apache.spark.rdd.RDD
```

如果不是使用Spark Shell的话，你需要一个`SparkContext`。
需要了解更多关于开始使用Spark的信息，
请参考[开始使用Spark指南](http://spark.apache.org/docs/latest/quick-start.html)。

# 属性图
<a name="property_graph"></a>

属性图是一种边和节点上可以附加用户定义对象的有向多重图。
有向多重图是指多条平行的边有可能共享同样的源节点和目的节点的有向图。
能够允许两个节点之间包含多条平行的边简化了建模，
在一些场景下同样的节点之间可能有多种关系（比如既是同事，又是朋友）。
每一个节点以一个64位的long(`VertexID`)作为id，
GraphX没有给节点的id强加任何有序性限制。
相似的，边有相应的源节点和目的节点id。

属性图有两个类型参数，节点类型（`VD`）和边类型（`ED`），
它们分别代表了关联到节点和边上的对象的类型。

> 当边类型或节点类型是简单的原始数据类型（比如int, double等）时，
> GraphX优化了它们的内部表示，通过存储为专门的数组来减少在内存中的印记。

在一些情况下可能需要让同一个图里节点的具有不同的属性类型，
这可以通过继承来实现。
比如建模一个包含用户（user）和商品（product）的二分图可以通过这样来实现：

```scala
class VertexProperty()
case class UserProperty(val name: String) extends VertexProperty
case class ProductProperty(val name: String, val price: Double) extends VertexProperty

// 这样，图就可以具有这样的类型：
var graph: Graph[VertexProperty, String] = null
```

和RDD一样，属性图是不可变的、分布式的和容错的。
改变图的值或者结构是通过新建转换之后的结果图来实现的。
注意到原来图的重要部分（没有改变的结构，属性和索引）在新的图中得到复用，
减少了这个功能性数据结构的开销。
通过一系列的启发式的节点划分方法把这个图划分到各个executor
（Spark中完成计算的基本单元）上去执行。
和RDD一样，图的任何一个划分（partition）都能够在发生错误的时候在另外一台机器上重新生成。

逻辑上，属性图对应了一对带有类型的集合（RDD），他们编码了每一个节点和边的属性。
因此，图这个类包含了边和节点作为它的成员：

```scala
class Graph[VD, ED] {
  val vertices: VertexRDD[VD]
  val edges: EdgeRDD[ED, VD]
}
```

类`VertexRDD[VD]`和类`EdgeRDD[ED, VD]`分别扩展了`RDD[(VertexID, VD)]`和`RDD[Edge[ED]]`，
并进行了一些优化。
围绕着图计算，
`VertexRDD[VD]`和`EdgeRDD[ED, VD]`提供了更多的功能，同时能够利用到内部的优化。
我们将在[节点和边RDD](#vertex_and_edge_rdds)一节更详细的讨论`VertexRDD`和`EdgeRDD`，
现在可以简单地把它们当成`RDD[(VertexID, VD)]`和`RDD[Edge[ED]]`。

### 属性图的例子

假设我们想要构建一个属性图，它包含GraphX这个项目的合作人员。
节点的属性包含用户名和职业，边的属性通过一个字符串来表示合作人员之间的关系。

![属性图示例](http://spark.apache.org/docs/latest/img/property_graph.png)

这个图具有如下的类型：

```scala
val userGraph: Graph[(String, String), String]
```

有很多的从原始文件、RDD、甚至是人造的生成器来构建图的方法，
这些将会在[图的构建器](#graph_builders)一节详细讨论。
或许最通用的方法是使用Graph这个对象，比如下面的代码从RDD构建一个图：

```scala
// 假设SparkContext已经被构建好
val sc: SparkContext
// 创建节点RDD
val users: RDD[(VertexId, (String, String))] =
  sc.parallelize(Array((3L, ("rxin", "student")), (7L, ("jgonzal", "postdoc")),
                       (5L, ("franklin", "prof")), (2L, ("istoica", "prof"))))
// 创建边RDD
val relationships: RDD[Edge[String]] =
  sc.parallelize(Array(Edge(3L, 7L, "collab"),    Edge(5L, 3L, "advisor"),
                       Edge(2L, 5L, "colleague"), Edge(5L, 7L, "pi")))
// 定义一个默认的用户，当有关系的用户缺失时使用这个用户
val defaultUser = ("John Doe", "Missing")
// 构建原始的图
val graph = Graph(users, relationships, defaultUser)
```

在上面的例子中，我们使用到了`Edge`这个case类，
Edge具有一个`srcId`和一个`dstId`，对应源节点和目的节点的id。
同时`Edge`类还有一个`attr`成员保存边的属性。

[Edge]: api/scala/index.html#org.apache.spark.graphx.Edge

我们能够通过`graph.vertices`和`graph.edges`成员从一个图中分解出相应的节点和边视图。

```scala
val graph: Graph[(String, String), String] // 上面构建的图
// 统计职业是postdoc的用户的个数
graph.vertices.filter { case (id, (name, pos)) => pos == "postdoc" }.count
// 同时src > dst的边的个数
graph.edges.filter(e => e.srcId > e.dstId).count
```

> 注意`graph.vertices`返回`VertexRDD[(String, String)]`，
> 它扩展了`RDD[(VertexID, (String, String))]`，我们通过scala的`case`表达式来分解元组。
> 另一方面，`graph.edges`返回`EdgeRDD`，它包含`Edge[String]`对象，
> 我们也可以使用下面的`case`类构建器：
> ```scala
graph.edges.filter { case Edge(src, dst, prop) => src > dst }.count
```

属性图除了具有节点视图和边视图之外，还提供了三元组（triplet）视图，
triplet视图在逻辑上join节点属性和边属性，生成`RDD[EdgeTriplet[VD, ED]]`，
后者是由`EdgeTriplet`实例组成的集合。
这个*join*可以通过下面的SQL表达式来表示：

```sql
SELECT src.id, dst.id, src.attr, e.attr, dst.attr
FROM edges AS e LEFT JOIN vertices AS src, vertices AS dst
ON e.srcId = src.Id AND e.dstId = dst.Id
```

或者用图表示为：

![边的三元组](http://spark.apache.org/docs/latest/img/triplet.png)

`EdgeTriplet`类扩展了`Edge`类，添加了`srcAttr`和`dstAttr`成员分别表示源节点和目的节点的属性。
我们可以通过一个图的triplet视图来给出一系列的字符串表示用户之间的关系：

```scala
val graph: Graph[(String, String), String] // 上面构建的图
// 使用triplet视图来生成事实的RDD
val facts: RDD[String] =
  graph.triplets.map(triplet =>
    triplet.srcAttr._1 + " is the " + triplet.attr + " of " + triplet.dstAttr._1)
facts.collect.foreach(println(_))
```

# 图的运算

就像`RDD`有像`map`，`filter`，`reduceByKey`这样的基本运算一样，
属性图也有一系列的基本运算，这些运算接受用户定义的函数，
然后生成另一个包含了转换之后属性或结构的图。
经过优化实现的核心运算定义在`Graph`类中，
一些方便的由核心运算组合而成的运算定义在`GraphOps`类中。
然而，由于Scala具有隐式转换这个特性，
定义在`GraphOps`中的运算能够自动地作为`Graph`的成员使用。
比如，我们可以通过下面的方式计算每一个节点的入度（定义在`GraphOps`中）:


```scala
val graph: Graph[(String, String), String]
// 使用隐式的GraphOps.inDegrees运算
val inDegrees: VertexRDD[Int] = graph.inDegrees
```

区分核心运算和`GraphOps`的原因在于将来可以支持不同的图的表示。
每一种图的表示必须提供核心运算的实现，然后重用很多定义在`GraphOps`中的有用的运算。

### 运算的总结列表

下面是定义在`Graph`和`GraphOps`中运算的一个快速总结，为了简洁，都把它放到了`Graph`这个类中。
注意一些函数得到了简化（比如默认参数和类型约束被移除），一些更高级的功能也被移除，
因此，请查阅API文档得到官方的运算列表。

```scala
/** 属性图的功能总结 */
class Graph[VD, ED] {
  // 图的信息 ===================================================================
  val numEdges: Long
  val numVertices: Long
  val inDegrees: VertexRDD[Int]
  val outDegrees: VertexRDD[Int]
  val degrees: VertexRDD[Int]
  // 图的视图 =============================================================
  val vertices: VertexRDD[VD]
  val edges: EdgeRDD[ED, VD]
  val triplets: RDD[EdgeTriplet[VD, ED]]
  // cache图的函数 ==================================================================
  def persist(newLevel: StorageLevel = StorageLevel.MEMORY_ONLY): Graph[VD, ED]
  def cache(): Graph[VD, ED]
  def unpersistVertices(blocking: Boolean = true): Graph[VD, ED]
  // 改变图的partition的启发式方法 ============================================================
  def partitionBy(partitionStrategy: PartitionStrategy): Graph[VD, ED]
  // 改变节点和边的属性 ==========================================================
  def mapVertices[VD2](map: (VertexID, VD) => VD2): Graph[VD2, ED]
  def mapEdges[ED2](map: Edge[ED] => ED2): Graph[VD, ED2]
  def mapEdges[ED2](map: (PartitionID, Iterator[Edge[ED]]) => Iterator[ED2]): Graph[VD, ED2]
  def mapTriplets[ED2](map: EdgeTriplet[VD, ED] => ED2): Graph[VD, ED2]
  def mapTriplets[ED2](map: (PartitionID, Iterator[EdgeTriplet[VD, ED]]) => Iterator[ED2])
    : Graph[VD, ED2]
  // 改变图的结构 ====================================================================
  def reverse: Graph[VD, ED]
  def subgraph(
      epred: EdgeTriplet[VD,ED] => Boolean = (x => true),
      vpred: (VertexID, VD) => Boolean = ((v, d) => true))
    : Graph[VD, ED]
  def mask[VD2, ED2](other: Graph[VD2, ED2]): Graph[VD, ED]
  def groupEdges(merge: (ED, ED) => ED): Graph[VD, ED]
  // Join其他的RDD和当前的图 ======================================================================
  def joinVertices[U](table: RDD[(VertexID, U)])(mapFunc: (VertexID, VD, U) => VD): Graph[VD, ED]
  def outerJoinVertices[U, VD2](other: RDD[(VertexID, U)])
      (mapFunc: (VertexID, VD, Option[U]) => VD2)
    : Graph[VD2, ED]
  // 关于邻接triplet的信息 =================================================
  def collectNeighborIds(edgeDirection: EdgeDirection): VertexRDD[Array[VertexID]]
  def collectNeighbors(edgeDirection: EdgeDirection): VertexRDD[Array[(VertexID, VD)]]
  def mapReduceTriplets[A: ClassTag](
      mapFunc: EdgeTriplet[VD, ED] => Iterator[(VertexID, A)],
      reduceFunc: (A, A) => A,
      activeSetOpt: Option[(VertexRDD[_], EdgeDirection)] = None)
    : VertexRDD[A]
  // 迭代图并行计算 ==========================================================
  def pregel[A](initialMsg: A, maxIterations: Int, activeDirection: EdgeDirection)(
      vprog: (VertexID, VD, A) => VD,
      sendMsg: EdgeTriplet[VD, ED] => Iterator[(VertexID,A)],
      mergeMsg: (A, A) => A)
    : Graph[VD, ED]
  // 基本图算法 ========================================================================
  def pageRank(tol: Double, resetProb: Double = 0.15): Graph[Double, Double]
  def connectedComponents(): Graph[VertexID, ED]
  def triangleCount(): Graph[Int, ED]
  def stronglyConnectedComponents(numIter: Int): Graph[VertexID, ED]
}
```


## 属性运算

和RDD `map`运算类似，
属性图也有下面的运算：

```scala
class Graph[VD, ED] {
  def mapVertices[VD2](map: (VertexId, VD) => VD2): Graph[VD2, ED]
  def mapEdges[ED2](map: Edge[ED] => ED2): Graph[VD, ED2]
  def mapTriplets[ED2](map: EdgeTriplet[VD, ED] => ED2): Graph[VD, ED2]
}
```

这些运算都产生一个新的图，在这个新图中节点或边的属性被用户定义的`map`函数改变。

> 注意，在上述所有的情况下图的结构都没有发生改变，
> 这是这些运算的关键特点。
> 它允许生成的图使用原来图的索引，
> 下面的两个代码段在逻辑上是等价的，
> 但是第一个不没有保持图的索引结构，因此不能利用到GraphX的系统优化：
>```scala
val newVertices = graph.vertices.map { case (id, attr) => (id, mapUdf(id, attr)) }
val newGraph = Graph(newVertices, graph.edges)
```
> 使用`mapVertices`来保持索引：
>```scala
val newGraph = graph.mapVertices((id, attr) => mapUdf(id, attr))
```

这些运算经常被用于初始化特定计算的图或者用于抛掉不必要的属性。
比如，给定一个出度作为节点属性的图（我们接下来会讲如何构建这样的图），
我们可以这样来初始化这个图用于PageRank:

```scala
// 给定一个节点属性是它的出度的图
val inputGraph: Graph[Int, String] =
  graph.outerJoinVertices(graph.outDegrees)((vid, _, degOpt) => degOpt.getOrElse(0))
// 构建一个图，在这个图中每一条边包含权重，
// 每一个节点值是初始的PageRank
val outputGraph: Graph[Double, Double] =
  inputGraph.mapTriplets(triplet => 1.0 / triplet.srcAttr).mapVertices((id, _) => 1.0)
```

## Structural Operators
## 结构性操作
<a name="structural_operators"></a>

现在GraphX仅支持少量经常被使用的结构性操作，
我们预测将来会支持更多。
下面是基本的结构性操作列表：

```scala
class Graph[VD, ED] {
  def reverse: Graph[VD, ED]
  def subgraph(epred: EdgeTriplet[VD,ED] => Boolean,
               vpred: (VertexId, VD) => Boolean): Graph[VD, ED]
  def mask[VD2, ED2](other: Graph[VD2, ED2]): Graph[VD, ED]
  def groupEdges(merge: (ED, ED) => ED): Graph[VD,ED]
}
```

`reverse`操作返回一个新的图， 这个图相对于原图所有边的方向都相反。
这个操作有时会非常有用，比如在计算反向PageRank时。
由于反向操作没有更改节点或者边的属性，也没有改变边的条数，
它能够被有效地实现，不需要数据的移动和复制。

`subgraph`运算接收判断边和节点的谓词，
然后生成一个新的图，这个图只包含满足谓词的节点，
只包含满足谓词*且关联节点满足谓词*的边。
`subgraph`运算在很多需要限制图只包含感兴趣节点和边或者需要移除破损的链接的情况下得到使用。
比如，在下面的代码中，我们移除破损的链接：


```scala
// 生成节点RDD
val users: RDD[(VertexId, (String, String))] =
  sc.parallelize(Array((3L, ("rxin", "student")), (7L, ("jgonzal", "postdoc")),
                       (5L, ("franklin", "prof")), (2L, ("istoica", "prof")),
                       (4L, ("peter", "student"))))
// 生成边的RDD
val relationships: RDD[Edge[String]] =
  sc.parallelize(Array(Edge(3L, 7L, "collab"),    Edge(5L, 3L, "advisor"),
                       Edge(2L, 5L, "colleague"), Edge(5L, 7L, "pi"),
                       Edge(4L, 0L, "student"),   Edge(5L, 0L, "colleague")))
// 定义一个默认的用户，当有关系的用户缺失时使用这个用户
val defaultUser = ("John Doe", "Missing")
// 构建初始的图
val graph = Graph(users, relationships, defaultUser)
// 注意到用户0（没有任何信息）链接到用户4（peter）和用户5（franklin）
graph.triplets.map(
    triplet => triplet.srcAttr._1 + " is the " + triplet.attr + " of " + triplet.dstAttr._1
  ).collect.foreach(println(_))
// 移除缺失的节点以及和它们相关联的边
val validGraph = graph.subgraph(vpred = (id, attr) => attr._2 != "Missing")
// validGraph不会包含用户4和5到用户0的边
validGraph.vertices.collect.foreach(println(_))
validGraph.triplets.map(
    triplet => triplet.srcAttr._1 + " is the " + triplet.attr + " of " + triplet.dstAttr._1
  ).collect.foreach(println(_))
```

> 注意到上面只提供了节点的谓词，
> `subgraph`在如果节点或者边的谓词没有提供的情况下，
> 默认把谓词当成`true`。

`mask`运算同样构建一个子图，在这个子图中只包含输入图中包含的节点和边。
这个运算可以和`subgraph`一起使用来根据另一个相关图的属性来限制一个图。
比如，我们可以在一个包含缺失节点的图上运行连通分量，
然后限制结果只包含有效子图的节点和边。


```scala
// 运行连通分量
val ccGraph = graph.connectedComponents() // No longer contains missing field
// 删除缺失节点以及和它们关联的边
val validGraph = graph.subgraph(vpred = (id, attr) => attr._2 != "Missing")
// 限制结果只包含有效子图的节点和边
val validCCGraph = ccGraph.mask(validGraph)
```

`groupEdges`运算把平行边（同一对节点之间的不同边）合并起来。
在很多数值应用中，平行边可以被*累加*（权值合并）到一条边从而减小图的尺寸。

## Join 运算
<a name="join_operators"></a>

在很多情况下，我们需要把外部集合（RDD）的数据和图join起来。
比如，我们可能有外部的用户属性，需要把这些属性合并到已经存在的图里；
或者我们想要从一个图中把节点属性拉到另外一个图中。
这些任务能够通过*join*运算实现。下面我们列出了主要的join运算：

```scala
class Graph[VD, ED] {
  def joinVertices[U](table: RDD[(VertexId, U)])(map: (VertexId, VD, U) => VD)
    : Graph[VD, ED]
  def outerJoinVertices[U, VD2](table: RDD[(VertexId, U)])(map: (VertexId, VD, Option[U]) => VD2)
    : Graph[VD2, ED]
}
```

`joinVertices`运算把节点和输入的RDD关联起来，返回一个新的图，
这个图的节点属性通过把用户定义的`map`函数应用的join之后的节点得到。
没有匹配到值的节点保持它原来的值。

> 注意到对于一个给定的节点，如果RDD中包含多个值那么只有一个会被用到。
> 因此，建议对输入的RDD先使用下面的方法进行一个去重的操作，
> 这个操作同时会对结果进行一次*预索引*，能够实质性地提升接下来join的速度：
> ```scala
val nonUniqueCosts: RDD[(VertexID, Double)]
val uniqueCosts: VertexRDD[Double] =
  graph.vertices.aggregateUsingIndex(nonUnique, (a,b) => a + b)
val joinedGraph = graph.joinVertices(uniqueCosts)(
  (id, oldCost, extraCost) => oldCost + extraCost)
```

更加通用的`outerJoinVertices`和`joinVertices`相似，
除了用户定义的`map`函数会被应用到所有的节点且能够改变节点的类型。
由于并不是所有的节点都会在输入的RDD中有一个匹配的值，
`map`函数接收的参数类型为`Option`。
比如我们能够通过把节点的属性初始化为它们的`outDegree`
来配置PageRank。

```scala
val outDegrees: VertexRDD[Int] = graph.outDegrees
val degreeGraph = graph.outerJoinVertices(outDegrees) { (id, oldAttr, outDegOpt) =>
  outDegOpt match {
    case Some(outDeg) => outDeg
    case None => 0 // 没有outDegree表示outDegree为0
  }
}
```

> 你可能会发现多参数列表（比如`f(a)(b)`）的柯里化函数模式在上述的例子中被使用。
> 尽管我们可以等价地把`f(a)(b)`写作`f(a, b)`，
> 但这意味着对于`b`的类型推导和`a`无关。
> 这样的结果是，用户需要给用户定义的函数提供类型标记：
> ```scala
val joinedGraph = graph.joinVertices(uniqueCosts,
  (id: VertexID, oldCost: Double, extraCost: Double) => oldCost + extraCost)
```


## 邻居聚合

图计算的一个重要部分是聚合每一个节点的邻居信息。
比如我们可能需要知道每一个用户的粉丝数目或者每一个用户粉丝的平均年龄。
许多迭代的图算法（比如PageRank，最短路径，连通分量）
重复的聚合邻居节点的属性（比如当前的PageRank值，到源节点的最短路径，最小可达节点id）。

### Map Reduce Triplets (mapReduceTriplets)
<a name="mrTriplets"></a>

在GraphX中，核心的（高度优化的）聚合原语是`mapReduceTriplets`运算：

```
class Graph[VD, ED] {
  def mapReduceTriplets[A](
      map: EdgeTriplet[VD, ED] => Iterator[(VertexId, A)],
      reduce: (A, A) => A)
    : VertexRDD[A]
}
```

`mapReduceTriplets`运算接收用户定义的`map`函数，它被应用到每一个triplet，
能够给triplet关联的两个节点中的任何一个发送*消息*（可以都发送，也可以都不发送）。
为了利用到预聚合的好处，我们现在只支持发送给源节点和目的节点的信息。
用户定义的`reduce`函数把发送给每一个节点的消息合并起来。
`mapReduceTriplets`运算返回一个`VertexRDD[A]`，
包含了对于每一个节点聚合之后的消息（类型为`A`）。
没有接收到消息的节点没有包含在返回的`VertexRDD`中。

> 注意到`mapReduceTriplets`接收一个可选的`activeSet`参数（上面没有写出，请查看API文档中的细节），
> 它限制了`map`阶段只应用到和提供的`activeSet`相关联的边:
> ```scala
  activeSetOpt: Option[(VertexRDD[_], EdgeDirection)] = None
```
> `EdgeDirection`指出和节点集合关联的那条边会被包含到`map`阶段。
> 如果`EdgeDirection`是`In`，那么`map`函数只会被应用到目的节点在`activeSet`中的边。
> 如果`EdgeDirection`是`Out`，那么`map`函数只会被应用到源节点在`activeSet`中的边。
> 如果`EdgeDirection`是`Either`，那么`map`函数只会被应用到源节点或目的节点在`activeSet`中的边。
> 如果`EdgeDirection`是`Both`，那么`map`函数只会被应用到源节点和目的节点都在`activeSet`中的边。
> 限制运算只在和部分节点相关的triplet上执行对于增量迭代计算很有必要，
> 它是在GraphX上实现Pregel的关键部分。

在下面的例子中，
我们使用`mapReduceTriplets`来计算对于每一个用户，
比他年长粉丝的平均年龄。

```scala
// 引入随机生成图的库
import org.apache.spark.graphx.util.GraphGenerators
// 创建一个"age"作为节点属性的图。为了简洁起见，这里使用一个随机图。
val graph: Graph[Double, Int] =
  GraphGenerators.logNormalGraph(sc, numVertices = 100).mapVertices( (id, _) => id.toDouble )
// 计算年长的粉丝的数目以及它们的总年龄
val olderFollowers: VertexRDD[(Int, Double)] = graph.mapReduceTriplets[(Int, Double)](
  triplet => { // Map 函数
    if (triplet.srcAttr > triplet.dstAttr) {
      // 把计数和年龄发给目的节点
      Iterator((triplet.dstId, (1, triplet.srcAttr)))
    } else {
      // 对于这种triplet不发送消息
      Iterator.empty
    }
  },
  // 把计数和年龄加起来
  (a, b) => (a._1 + b._1, a._2 + b._2) // Reduce 函数
)
// 用总年龄除以数目得到平均年龄
val avgAgeOfOlderFollowers: VertexRDD[Double] =
  olderFollowers.mapValues( (id, value) => value match { case (count, totalAge) => totalAge / count } )
// 展示结果
avgAgeOfOlderFollowers.collect.foreach(println(_))
```

> 注意到当消息（和消息的总和）的大小是一个常数时（floats和加法，而不是list和链接），
> `mapReduceTriplets`表现得最优。
> 更准确的说，`mapReduceTriplets`的结果在理想情况下应该相对于节点的度是次线性的（sub-linear）。

### 计算度信息

一种通用的聚合任务是计算每一个节点的度：每一个节点邻接边的条数。
在有向图的情况下需要知道出度、入度和全度。
`GraphOps`类包含了一系列计算节点度的运算。
比如，在下面我们计算最大的出度、入度和全度：

```scala
// 定义一个计算最高的度的reduce函数
def max(a: (VertexId, Int), b: (VertexId, Int)): (VertexId, Int) = {
  if (a._2 > b._2) a else b
}
// 计算最高度
val maxInDegree: (VertexId, Int)  = graph.inDegrees.reduce(max)
val maxOutDegree: (VertexId, Int) = graph.outDegrees.reduce(max)
val maxDegrees: (VertexId, Int)   = graph.degrees.reduce(max)
```

### 收集邻居

有时通过收集邻居节点及它们的属性来表示计算可能更容易。
这可以通过`collectNeighborIds`和`collectNeighbors`很容易地实现。


```scala
class GraphOps[VD, ED] {
  def collectNeighborIds(edgeDirection: EdgeDirection): VertexRDD[Array[VertexId]]
  def collectNeighbors(edgeDirection: EdgeDirection): VertexRDD[ Array[(VertexId, VD)] ]
}
```

> 注意到这些运算可能会有很大的开销因为它们复制了信息而且需要大量的通信。
> 尽可能地直接使用`mapReduceTriplets`来表达同样的计算。

## Caching 和 Uncaching

在Spark中，RDD默认不持久化到内存。
为了避免重复计算，
当需要被重复使用时，它们必须显式的缓存起来
（参见[Spark编程指南](http://spark.apache.org/docs/latest/programming-guide.html#rdd-persistence)）。
在GraphX中的图也是按照同样的机制来处理。
**当一个图被重复使用时，确保它一开始调用了`Graph.cache()`。**

在迭代计算时，*uncaching*对于获取最佳的性能也很有必要。
在默认情况下，缓存的RDD和图会一直保存在内存中，
直到内存压力把它按照LRU的顺序从内存中移出。
对于迭代计算，上一轮迭代的中间结果很快会填满缓存。
尽管它们最终会被移出，没必要的数据保存在内存中会减慢垃圾回收的速度。
当中间结果不需要时立刻uncache能够有效地利用内存。
这包括在每一轮迭代时物化图和RDD，uncache其他的数据集，
并在将来的迭代中只使用物化的数据集。
然而，由于图有多个RDD构成，正确的unpersist它有点困难。
**对于迭代计算，我们推荐使用Pregel API，它能够正确的unpersist中间结果。**

# Pregel API
<a name="pregel"></a>

图是一种固有的递归数据结构，因为节点的属性依赖于它们的邻居节点的属性，
而它们邻居节点的属性又依赖于*邻居节点的邻居节点*的属性。
因此，很多重要的图算法不断的计算每一个节点的属性直到一个固定的条件得到满足。
为了表达这些迭代算法，一系列的图并行抽象被提出。
GraphX提供了一个类似于Pregel的运算，它是广泛使用的Pregel和Graphlab的一个融合产物。

在更高的层次上，GraphX中的Pregel运算是一种整体同步并行（Bulk Synchronous Parallel）消息抽象，
它受限于图的拓扑结构。
Pregel运算执行一系列的超步（super-step），在每一个超步中节点接收上一个超步发给它的消息的*聚合*，
重新计算新的节点属性值，然后发送消息给它的邻居节点用于下一个超步的计算。
和Pregel不一样，而是更像Graphlab，消息通过triplet的函数并行计算，
而且在计算消息的时候能够获取源节点和目的节点的属性。
没有接收到消息的节点在下一个超步中会跳过计算。
当没有消息在传递时，Pregel运算停止，返回最终的图。

> 注意，和标准的Pregel实现不一样，在GraphX中节点只可以给它的邻居节点发送消息，
> 而且消息的构建通过一个用户定义的消息函数并行的执行。
> 这些限制允许在GraphX进行更多的优化。

下面是`Pregel`运算的类型签名和一个骨架实现（注意调用`graph.cache`被移除）：

```scala
class GraphOps[VD, ED] {
  def pregel[A]
      (initialMsg: A,
       maxIter: Int = Int.MaxValue,
       activeDir: EdgeDirection = EdgeDirection.Out)
      (vprog: (VertexId, VD, A) => VD,
       sendMsg: EdgeTriplet[VD, ED] => Iterator[(VertexId, A)],
       mergeMsg: (A, A) => A)
    : Graph[VD, ED] = {
    // 对于每一个节点接收初始的消息
    var g = mapVertices( (vid, vdata) => vprog(vid, vdata, initialMsg) ).cache()
    // 计算消息
    var messages = g.mapReduceTriplets(sendMsg, mergeMsg)
    var activeMessages = messages.count()
    // 不断循环，直到没有消息传递或者达到了最大的迭代次数
    var i = 0
    while (activeMessages > 0 && i < maxIterations) {
      // 接收消息: -----------------------------------------------------------------------
      // 在接收消息的所有节点上运行节点程序
      val newVerts = g.vertices.innerJoin(messages)(vprog).cache()
      // 把新的节点值合并到原图
      g = g.outerJoinVertices(newVerts) { (vid, old, newOpt) => newOpt.getOrElse(old) }.cache()
      // 发送消息: ------------------------------------------------------------------------------
      // 没有接收到消息的节点不会出现在newVerts中，因此不会发送消息。
      // 更精确地说，
      // mapReduceTriplets的map阶段只会在和同时在activeDir和newVerts的节点相关的边上触发
      messages = g.mapReduceTriplets(sendMsg, mergeMsg, Some((newVerts, activeDir))).cache()
      activeMessages = messages.count()
      i += 1
    }
    g
  }
}
```

注意到Pregel接收两个参数列表（即`graph.pregel(list1)(list2)`）。
第一个参数列表包含配置参数包括初始消息，
最大迭代次数和发送消息的边的方向（默认两个方向都发）。
第二个参数列表包含用户定义的接收消息的函数（节点函数`vprog`），
计算消息的函数（`sendMsg`），以及合并消息的函数（`mergeMsg`）。

我们能够使用Pregel运算来表达运算，
比如在下面的最短路劲的例子：

```scala
import org.apache.spark.graphx._
// 引入随机生成图的库
import org.apache.spark.graphx.util.GraphGenerators
// 一个边的属性包含距离的图
val graph: Graph[Int, Double] =
  GraphGenerators.logNormalGraph(sc, numVertices = 100).mapEdges(e => e.attr.toDouble)
val sourceId: VertexId = 42 // The ultimate source
// 初始化图，使得除了源节点，所有的节点的距离为无穷大
val initialGraph = graph.mapVertices((id, _) => if (id == sourceId) 0.0 else Double.PositiveInfinity)
val sssp = initialGraph.pregel(Double.PositiveInfinity)(
  (id, dist, newDist) => math.min(dist, newDist), // 节点程序
  triplet => {  // 发送消息
    if (triplet.srcAttr + triplet.attr < triplet.dstAttr) {
      Iterator((triplet.dstId, triplet.srcAttr + triplet.attr))
    } else {
      Iterator.empty
    }
  },
  (a,b) => math.min(a,b) // 合并消息
  )
println(sssp.vertices.collect.mkString("\n"))
```

# 图构建器
<a name="graph_builders"></a>

GraphX提供了很多的从节点和边的集合（RDD或者磁盘文件）中构建图的方法。
这些构建器默认都不会重新划分图的边，反过来，这些边会保留它们原来的划分（比如它们原来在HDFS中的分块）。
`Graph.groupEdges`要求图被重新划分，因为它假设相同的边会被放置在同一个划分，
所以在调用`groupEdges`之间必须调用`Graph.partitionBy`。

```scala
object GraphLoader {
  def edgeListFile(
      sc: SparkContext,
      path: String,
      canonicalOrientation: Boolean = false,
      minEdgePartitions: Int = 1)
    : Graph[Int, Int]
}
```

`GraphLoader.edgeListFile`提供了一种从磁盘上的边列表上加载图的方法。
它解析格式为下面的（源节点id, 目的节点id）对的邻接列表，跳过以`#`开头的注释行：

~~~
# This is a comment
2 1
4 1
1 2
~~~

它从上面指定的边中新建一个图，自动创建边关联的节点。所有的节点和边属性默认为1。
`canonicalOrientation`参数允许重新调整边的方向为正向（`srcId < dstId`， 源节点的id小于目的节点id），
这对于连通分量算法来说是必须的。
`minEdgePartitions`参数指定了最少需要生成多少个边的划分，
可能会生成比指定更多的边划分，比如在HDFS文件有更多分块的情况下。

```scala
object Graph {
  def apply[VD, ED](
      vertices: RDD[(VertexId, VD)],
      edges: RDD[Edge[ED]],
      defaultVertexAttr: VD = null)
    : Graph[VD, ED]

  def fromEdges[VD, ED](
      edges: RDD[Edge[ED]],
      defaultValue: VD): Graph[VD, ED]

  def fromEdgeTuples[VD](
      rawEdges: RDD[(VertexId, VertexId)],
      defaultValue: VD,
      uniqueEdges: Option[PartitionStrategy] = None): Graph[VD, Int]

}
```


`Graph.apply`允许通过从节点和边的RDD构建图。对重复的节点从中任意挑选一个，
对于在边的RDD中出现但在节点RDD中没有出现的节点被赋予默认的属性值。

`Graph.fromEdges`允许仅仅从边的RDD构建图，自动构建出现的节点并赋予它们默认的属性。

`Graph.fromEdgeTuples`允许边元组的RDD构建图，给每一条边赋予值1，自动构建出现的节点并赋予它们默认的属性值。
它同时支持对边的去重；为了去重，传入一个`PartitionStrategy`的`Some`作为`uniqueEdges`参数
（比如`uniqueEdges = Some(PartitionStrategy.RandomVertexCut)`）。
需要划分策略来把重复的边放在同一个划分，这样才能去重。

# Vertex and Edge RDDs
# 节点和边RDD
<a name="vertex_and_edge_rdds"></a>

GraphX提供对于图的节点和边的RDD视图。
然而，由于GraphX维持了节点和边的优化的数据结构，
而且这些数据结构提供了更多的功能，
节点和边分别通过`VertexRDD`和`EdgeRDD`返回。
在这个部分我们考察这些类型的一些有用的功能。

## VertexRDDs

`VertexRDD[A]`扩展了`RDD[(VertexID, A)]`而且限制了每一个`VertexID`只会出现*一次*。
同时，`VertexRDD[A]`表示了*一系列*属性类型是`A`的节点。
在内部，这通过把节点属性保存在一个可复用的哈希表来实现。
因此，如果两个`VertexRDD`来源于同一个基础`VertexRDD`（比如通过`filter`或`mapValues`）,
那么它们能够在常数时间内完成join，不需要hash计算。
为了利用这个索引数据结构，`VertexRDD`提供了下面的功能：

```scala
class VertexRDD[VD] extends RDD[(VertexID, VD)] {
  // 过滤节点集，但保留内部索引
  def filter(pred: Tuple2[VertexId, VD] => Boolean): VertexRDD[VD]
  // 改变节点值而不改变id（保留内部索引）
  def mapValues[VD2](map: VD => VD2): VertexRDD[VD2]
  def mapValues[VD2](map: (VertexId, VD) => VD2): VertexRDD[VD2]
  // 把出现在other集合中的节点从当前集合中删除
  def diff(other: VertexRDD[VD]): VertexRDD[VD]
  // 利用内部索引来（实质性地）加速的join运算
  def leftJoin[VD2, VD3](other: RDD[(VertexId, VD2)])(f: (VertexId, VD, Option[VD2]) => VD3): VertexRDD[VD3]
  def innerJoin[U, VD2](other: RDD[(VertexId, U)])(f: (VertexId, VD, U) => VD2): VertexRDD[VD2]
  // 使用当前RDD索引来加速输入RDD的reduceByKey运算
  def aggregateUsingIndex[VD2](other: RDD[(VertexId, VD2)], reduceFunc: (VD2, VD2) => VD2): VertexRDD[VD2]
}
```

需要注意的是，比如对于`filter`运算，如何返回一个`VertexRDD`。
过滤实际上是通过`BitSet`来实现，因此复用了索引，保持了和其他`VertexRDD`做快速join的能力。
同样的，`mapValues`不允许`map`函数改变`VertexID`，
因此是同样的`HashMap`能够被服用。
`leftJoin`和`innerJoin`都能够鉴别出两个`VertexRDD`来源于同一个`HashMap`，
然后通过线性的扫描而不是耗时的点扫描来实现join。

`aggregateUsingIndex`运算对于高效地从一个`RDD[(VertexID, A)]`构建一个新的`VertexRDD`很有用。
从概念上讲，如果我们构建了一个`VertexRDD[B]`，
它包含的节点是某个`RDD[(VertexID, A)]`的超集，
那我们可以使用这个索引来聚合和接下来索引`RDD[(VertexID, A)]`。
比如：

```scala
val setA: VertexRDD[Int] = VertexRDD(sc.parallelize(0L until 100L).map(id => (id, 1)))
val rddB: RDD[(VertexId, Double)] = sc.parallelize(0L until 100L).flatMap(id => List((id, 1.0), (id, 2.0)))
// 在rddB中200条记录
rddB.count
val setB: VertexRDD[Double] = setA.aggregateUsingIndex(rddB, _ + _)
// 在rddB中100条记录
setB.count
// 现在join A和B会很快！
val setC: VertexRDD[Double] = setA.innerJoin(setB)((id, a, b) => a + b)
```

## EdgeRDDs

`EdgeRDD[ED, VD]`扩展了`RDD[Edge[ED]]`，
通过`PartitionStrategy`中定义的划分策略中的一个来组成边位一个个的分块。
在每一个分块内部，边的属性和邻接结构分开村春，
从而能够在属性改变的时候最大重用。

`EdgeRDD`提供的三个增加的功能是：

```scala
// 改变边的属性值，同时保持结构
def mapValues[ED2](f: Edge[ED] => ED2): EdgeRDD[ED2, VD]
// 翻转边的方向，重用属性和结构
def reverse: EdgeRDD[ED, VD]
// Join两个使用同样划分策略划分的EdgeRDD
def innerJoin[ED2, ED3](other: EdgeRDD[ED2, VD])(f: (VertexId, VertexId, ED, ED2) => ED3): EdgeRDD[ED3, VD]
```

在大多数情况下我们发现对于`EdgeRDD`的运算可以通过图运算或者基础的`RDD`类中定义的运算来实现。

# 优化表示

尽管详细地描述GraphX表示分布式的图的优化已经超出这份指南的范围，
一些高层次的理解能够帮助设计可扩展的算法和优化API的使用。
GraphX采用节点分割（vertex-cut）的方法来对分布式的图进行划分。

![边分割 VS 节点分割](http://spark.apache.org/docs/latest/img/edge_cut_vs_vertex_cut.png)

GraphX在通过节点对图进行划分而不是通过边进行划分，
这样能够减少通信和存储的开销。
从逻辑上说，这种方法把每一条边分配到某一台机器，
然后允许一个节点跨越多台机器。
具体的边分配策略取决于`PartitionStrategy`，
在不同的启发式策略中有很多的取舍。
用户可以使用`Graph.partitionBy`重新划分这个图，从而在不同的策略中选择。
默认的划分策略是使用在图构建中边的初始划分。
然而，用户可以切换到2D-划分和在GraphX中包含的启发式划分策略。

![RDD图的表示](http://spark.apache.org/docs/latest/img/vertex_routing_edge_tables.png)

在边划分好之后，高效的图并行计算的主要挑战在于有效地join节点的属性和边的属性。
因为显示中的图的边的个数多于节点的个数，
我们把节点的属性移到边上（进行join）。
因为并不是所有的划分都能保证边和节点相邻，
在实现类似`triplets`或`mapReduceTriplets`运算需要join时，
我们在内部维护了一张路由表记录每一个节点应该被广播到哪些机器上。

# 图算法
<a name="graph_algorithms"></a>

GraphX包含了一系列的图算法来简化分析的任务。
这些算法包含在包`org.apache.spark.graphx.lib`中，
能够直接通过`GraphOps`作为`Graph`的方法使用。
在这个部分中描述这些算法以及这些算法是如何被使用的。

## PageRank
<a name="pagerank"></a>

PageRank计算每一个节点在图中的重要性，假设一条从*u*到*v*的边表示*u*给*v*的重要性投了一票。
如果一个Twitter用户被很多其他用户follow，那么这个用户的排名会很高。

GraphX提供了静态和动态的PageRank实现，作为`PageRank`对象的方法。
静态的PageRank迭代固定的次数，而动态的PageRank算法不断地运行直到排名（rank值）收敛
（也就是改变的值在特定承受范围之内）。
`GraphOps`允许直接作为`Graph`的方法调用这些函数。

GraphX也包含了一个示例社交网络数据集，我们可以在这上面运行PageRank算法。
在`graphx/data/user.txt`中给出了一个用户集合，
在`graphx/data/followers.txt`中给出了一个关系集合。
我们通过下面的方式计算每一个用户的PageRank值：

```scala
// 加载边来构建图
val graph = GraphLoader.edgeListFile(sc, "graphx/data/followers.txt")
// 运行PageRank
val ranks = graph.pageRank(0.0001).vertices
// 把rank值和用户名关联起来
val users = sc.textFile("graphx/data/users.txt").map { line =>
  val fields = line.split(",")
  (fields(0).toLong, fields(1))
}
val ranksByUsername = users.join(ranks).map {
  case (id, (username, rank)) => (username, rank)
}
// 打印结果
println(ranksByUsername.collect().mkString("\n"))
```

## 连通分量

连通分量算法以连通分量中最小的节点id作为这个连通分量的标签。
比如，在一个社交网络中，连通分量可以近似为一个聚类。
GraphX在`ConnectedComponents`中包含了这个算法的一个实现，
我们计算在PageRank节给出的数据集上计算连通分量如下：

```scala
// 和在PageRank的例子中一样，从数据集中加载图
val graph = GraphLoader.edgeListFile(sc, "graphx/data/followers.txt")
// 找到连通分量
val cc = graph.connectedComponents().vertices
// 把连通分量和用户名关联起来
val users = sc.textFile("graphx/data/users.txt").map { line =>
  val fields = line.split(",")
  (fields(0).toLong, fields(1))
}
val ccByUsername = users.join(cc).map {
  case (id, (username, cc)) => (username, cc)
}
// 打印结果
println(ccByUsername.collect().mkString("\n"))
```

## 三角形计数

当一个节点的两个邻居之间有一条边，那一个节点就是一个三角形的一部分。
GraphX在`TriangleCount`对象中实现了一个三角形计数的算法，
它能够统计出经过每一个节点的三角形的数目，
提供一个测量聚类的方法。
我们在PageRank节的社交网络数据集上计算三角形的数目。
** 注意`TriangleCount`需要节点的边是按照标准的方向（`srcId < dstId`）而且图通过`Graph.partitionBy`划分。 **

```scala
// 按照标准的方向加载边并划分图
val graph = GraphLoader.edgeListFile(sc, "graphx/data/followers.txt", true).partitionBy(PartitionStrategy.RandomVertexCut)
// 找到每一个节点的三角形个数
val triCounts = graph.triangleCount().vertices
// 把三角形个数和用户名关联起来
val users = sc.textFile("graphx/data/users.txt").map { line =>
  val fields = line.split(",")
  (fields(0).toLong, fields(1))
}
val triCountByUsername = users.join(triCounts).map { case (id, (username, tc)) =>
  (username, tc)
}
// 打印结果
println(triCountByUsername.collect().mkString("\n"))
```

# 示例

假设我想要从文本文件构建一个图，
限制图只包含重要的关系和用户，
在这个子图上面运行PageRank，
最后返回头几位（top）用户的属性。
使用GraphX，只需要很少行数的代码就可以完成所有的这些事情：

```scala
// 连接到Spark集群
val sc = new SparkContext("spark://master.amplab.org", "research")

// 加载用户数据然后解析成用户id和属性列表
val users = (sc.textFile("graphx/data/users.txt")
  .map(line => line.split(",")).map( parts => (parts.head.toLong, parts.tail) ))

// 解析边的数据（数据格式已经是userId -> userId）
val followerGraph = GraphLoader.edgeListFile(sc, "graphx/data/followers.txt")

// 附加用户属性
val graph = followerGraph.outerJoinVertices(users) {
  case (uid, deg, Some(attrList)) => attrList
  // 有些用户可能没有属性，我们把它们设为空
  case (uid, deg, None) => Array.empty[String]
}

// 限制图只包含具有用户名和名字的用户
val subgraph = graph.subgraph(vpred = (vid, attr) => attr.size == 2)

// 计算PageRank
val pagerankGraph = subgraph.pageRank(0.001)

// 得到top用户的属性
val userInfoWithPageRank = subgraph.outerJoinVertices(pagerankGraph.vertices) {
  case (uid, attrList, Some(pr)) => (pr, attrList.toList)
  case (uid, attrList, None) => (0.0, attrList.toList)
}

println(userInfoWithPageRank.vertices.top(5)(Ordering.by(_._2._1)).mkString("\n"))
```
