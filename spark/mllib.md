## MLlib的实现

在Python中提供了MLlib的接口，
用户可以使用Spark中的MLLib模块提供的算法。
下面分析一下实现这个模块的细节。

### 交互

以Kmeans这个算法为例，用户调用的示例为：

```python
>>> data = array([0.0,0.0, 1.0,1.0, 9.0,8.0, 8.0,9.0]).reshape(4,2)
>>> model = KMeans.train(
...     sc.parallelize(data), 2, maxIterations=10, runs=30, initializationMode="random")
>>> model.predict(array([0.0, 0.0])) == model.predict(array([1.0, 1.0]))
True
```

首先，用户调用`Kmeans.train`得到训练的模型，`Kmeans.train`实现代码如下：

```python
class KMeans(object):
    @classmethod
    def train(cls, data, k, maxIterations=100, runs=1, initializationMode="k-means||"):
        """Train a k-means clustering model."""
        sc = data.context
        dataBytes = _get_unmangled_double_vector_rdd(data)
        ans = sc._jvm.PythonMLLibAPI().trainKMeansModel(
            dataBytes._jrdd, k, maxIterations, runs, initializationMode)
        if len(ans) != 1:
            raise RuntimeError("JVM call result had unexpected length")
        elif type(ans[0]) != bytearray:
            raise RuntimeError("JVM call result had first element of type "
                               + type(ans[0]) + " which is not bytearray")
        matrix = _deserialize_double_matrix(ans[0])
        return KMeansModel([row for row in matrix])
```

首先把数据序列化成RDD，然后调用`PythonMLLibAPI`提供的`trainKMeansModel`接口，
得到训练之后的结果（类型为`java.util.List[java.lang.Object]`），
然后把这个结果反序列化为矩阵，封装成`KMeansModel`模型。

`PythonMLlibAPI.trainKMeansModel`的实现代码如下：

```scala
def trainKMeansModel(
      dataBytesJRDD: JavaRDD[Array[Byte]],
      k: Int,
      maxIterations: Int,
      runs: Int,
      initializationMode: String): java.util.List[java.lang.Object] = {
    val data = dataBytesJRDD.rdd.map(bytes => deserializeDoubleVector(bytes))
    val model = KMeans.train(data, k, maxIterations, runs, initializationMode)
    val ret = new java.util.LinkedList[java.lang.Object]()
    ret.add(serializeDoubleMatrix(model.clusterCenters.map(_.toArray)))
    ret
  }
```

先反序列化输入，然后调用Spark的MLlib中的Kmeans训练模型，
得到结果之后序列化成`java.util.List`，返回给Python代码。

### 向量运算的通用接口

PySpark支持下面的向量：

* Numpy中的array
* Python的list
* Mlib中的SparseVector
* Scipy中的单列的`csc_matrix`

对于这些向量，在`_common.py`提供两个通用的函数:

* `_squared_distance`: 用于计算两个向量的距离。
* `_dot`: 用于计算向量的乘积。

所有类型的向量都可以使用这两个函数来进行计算。
